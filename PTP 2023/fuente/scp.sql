CREATE TABLE "scp_config" (
	"id"	INTEGER NOT NULL,
	"param"	VARCHAR(150) NOT NULL UNIQUE,
	"value1"	VARCHAR(254) NOT NULL,
	"value2"	VARCHAR(254),
	"value3"	VARCHAR(254),
	PRIMARY KEY("id" AUTOINCREMENT)
);

INSERT INTO scp_config VALUES(null, "tema", "System", null, null);


CREATE TABLE "scp_producto" (
	"id"	INTEGER NOT NULL,
	"nombre"	VARCHAR(254) NOT NULL,
	"referencia"	VARCHAR(254) NOT NULL UNIQUE,
	"tiempo_estandar"	REAL,
	PRIMARY KEY("id" AUTOINCREMENT)
);


CREATE TABLE "scp_material" (
	"id"	INTEGER NOT NULL,
	"nombre_tipo"	VARCHAR(254) NOT NULL,
	"proveedor"	VARCHAR(254),
	"unidad_med"	VARCHAR(50) NOT NULL,
	"costo_unitario"	REAL NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);


CREATE TABLE "scp_producto_material" (
	"id"	INTEGER NOT NULL,
	"producto_id"	INTEGER NOT NULL,
	"material_id"	INTEGER NOT NULL,
	"consumo_unit"	INTEGER NOT NULL,
	FOREIGN KEY("material_id") REFERENCES "scp_material"("id") ON DELETE NO ACTION,
	FOREIGN KEY("producto_id") REFERENCES "scp_producto"("id") ON DELETE NO ACTION,
	PRIMARY KEY("id" AUTOINCREMENT),
	UNIQUE("producto_id","material_id")
);