from tkinter import *

def siguiente():
    root.title('Siguiente')

def anterior():
    root.title('Anterior')

def casa():
    root.title('Casa')

root = Tk()

w = Label(root, text="Right-click to display menu", width=40, height=20)
w.pack()

# create a menu
popup = Menu(root, tearoff=0)
popup.add_command(label="Next", command=siguiente) # , command=next) etc...
popup.add_command(label="Previous", command=anterior)
popup.add_separator()
popup.add_command(label="Home", command=casa)

def do_popup(event):
    # display the popup menu
    try:
        popup.tk_popup(event.x_root, event.y_root, 0)
    finally:
        # make sure to release the grab (Tk 8.0a1 only)
        popup.grab_release()
#        return None
    pass


#TODO:Detectar el SO, el original usaba "<Button-3>", que no funciona en macOS.
w.bind("<Button-2>", do_popup)

b = Button(root, text="Quit", command=root.destroy)
b.pack()

root.mainloop()