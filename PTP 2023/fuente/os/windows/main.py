from customtkinter import *
from tkinter import messagebox, PhotoImage
import tkinter as tk
from tkinter import ttk
import sqlite3
from PIL import Image
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent.parent


class App(CTk):
    def __init__(self):
        super().__init__()

        self.geometry("1350x660+0+0")

        self.conexion = False
        #icon
        self.iconpath = PhotoImage(file=BASE_DIR/"favicon_io/logo32.png")
        self.wm_iconbitmap()
        self.iconphoto(False, self.iconpath)

        # menu de plataforma macOS
        plattform = tk.Menu(self)

        self.selected_option = tk.StringVar()
        # fin - menu

        # menu
        # menubar = tk.Menu(self)

        file_menu = tk.Menu(plattform, tearoff=0)
        file_menu.add_command(label="Exportar datos...")
        file_menu.add_command(label="Importar datos")
        file_menu.add_separator()
        file_menu.add_command(label="Hoja de Costos")
        file_menu.add_separator()
        file_menu.add_command(label="Salir", command=self.quit)

        edit_menu = tk.Menu(plattform, tearoff=0)
        edit_menu.add_command(label="Productos")
        edit_menu.add_command(label="Materiales")

        conf_menu = tk.Menu(plattform, tearoff=0)
        conf_menu.add_radiobutton(label="Tema Claro", variable=self.selected_option, value="Light",
                                  command=self.change_appearance_mode_event)
        conf_menu.add_radiobutton(label="Tema Oscuro", variable=self.selected_option, value="Dark",
                                  command=self.change_appearance_mode_event)
        conf_menu.add_radiobutton(label="Tema Sistema", variable=self.selected_option, value="System",
                                  command=self.change_appearance_mode_event)
        conf_menu.add_separator()
        conf_menu.add_command(label="Carga Prestacional", command=self.carga_prestacional_event)

        help_menu = tk.Menu(plattform, tearoff=0)
        help_menu.add_command(label="Ayuda", accelerator="Control+A", command=self.help_menu_event)
        help_menu.add_separator()
        help_menu.add_command(label="Acerca de...", accelerator="Control+X", command=self.help_menu_about_event)
        self.bind_all("<Control-X>", self.help_menu_event)

        plattform.add_cascade(label="Archivo", menu=file_menu)
        plattform.add_cascade(label="Editar", menu=edit_menu)
        plattform.add_cascade(label="Configuración", menu=conf_menu)
        plattform.add_cascade(label="Ayuda", menu=help_menu)

        self.config(menu=plattform)

        # fin - menu

        # set grid layout 1x2
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)

        self.logo_image = CTkImage(Image.open(BASE_DIR / "favicon_io/logo32.png"), size=(32, 32))
        self.large_test_image = CTkImage(Image.open(BASE_DIR / "test_images/logoSena.png"), size=(180, 176))
        self.image_icon_image = CTkImage(Image.open(BASE_DIR / "test_images/image_icon_light.png"), size=(20, 20))
        self.home_image = CTkImage(light_image=Image.open(BASE_DIR / "test_images/home_dark.png"),
                                   dark_image=Image.open(BASE_DIR / "test_images/home_light.png"), size=(20, 20))
        self.chat_image = CTkImage(light_image=Image.open(BASE_DIR / "test_images/chat_dark.png"),
                                   dark_image=Image.open(BASE_DIR / "test_images/chat_light.png"), size=(20, 20))
        self.add_user_image = CTkImage(light_image=Image.open(BASE_DIR / "test_images/add_user_dark.png"),
                                       dark_image=Image.open(BASE_DIR / "test_images/add_user_light.png"),
                                       size=(20, 20))

        # create navigation frame
        self.navigation_frame = CTkFrame(self, corner_radius=10)
        self.navigation_frame.grid(row=0, column=0, sticky="nsew")
        self.navigation_frame.grid_rowconfigure(7, weight=1)

        self.navigation_frame_label = CTkLabel(self.navigation_frame, text="  SIMULADOR\n  Costos de Producción",
                                               image=self.logo_image,
                                               compound="left", font=CTkFont(size=15, weight="bold"))
        self.navigation_frame_label.grid(row=0, column=0, padx=20, pady=20)

        self.home_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                     text="Inicio",
                                     fg_color="transparent", text_color=("gray10", "gray90"),
                                     hover_color=("gray70", "gray30"),
                                     image=self.home_image, anchor="w", command=self.home_button_event)
        self.home_button.grid(row=1, column=0, sticky="ew")

        self.productos_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                        text="Productos",
                                        fg_color="transparent", text_color=("gray10", "gray90"),
                                        hover_color=("gray70", "gray30"),
                                        image=self.chat_image, anchor="w", command=self.productos_menu_event)
        self.productos_menu.grid(row=2, column=0, sticky="ew")

        self.materiales_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                        text="Materiales",
                                        fg_color="transparent", text_color=("gray10", "gray90"),
                                        hover_color=("gray70", "gray30"),
                                        image=self.chat_image, anchor="w", command=self.materiales_menu_event)
        self.materiales_menu.grid(row=3, column=0, sticky="ew")

        self.frame_3_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                        text="Hoja de Costos",
                                        fg_color="transparent", text_color=("gray10", "gray90"),
                                        hover_color=("gray70", "gray30"),
                                        image=self.add_user_image, anchor="w", command=self.frame_3_button_event)
        self.frame_3_button.grid(row=4, column=0, sticky="ew")

        self.carga_prestacional_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                                 text="Carga Prestacional",
                                                 fg_color="transparent", text_color=("gray10", "gray90"),
                                                 hover_color=("gray70", "gray30"),
                                                 image=self.add_user_image, anchor="w", command=self.carga_prestacional_event)
        self.carga_prestacional_menu.grid(row=5, column=0, sticky="ew")
        self.salario_min = 0
        self.aux_transporte = 0
        self.prima_servicios = 0
        self.aux_cesantias = 0
        self.intereses_cesantias = 0
        self.vacaciones = 0
        self.caja_compensacion = 0
        self.pension = 0
        self.arl = 0
        self.salud = 0
        self.total_costo_mensual_trabajador = 0


        self.frame_5_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                        text="Valor minuto",
                                        fg_color="transparent", text_color=("gray10", "gray90"),
                                        hover_color=("gray70", "gray30"),
                                        image=self.add_user_image, anchor="w", command=self.frame_5_button_event)
        self.frame_5_button.grid(row=6, column=0, sticky="ew")

        self.appearance_mode_menu = CTkOptionMenu(self.navigation_frame, values=["Light", "Dark", "System"],
                                                  command=self.change_appearance_mode_event)
        self.appearance_mode_menu.grid(row=7, column=0, padx=20, pady=20, sticky="s")

        # create home frame
        self.home_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.home_frame.grid_columnconfigure(0, weight=1)

        self.home_frame_large_image_label = CTkLabel(self.home_frame,
                                                     text="\nProducto Técnico-Pedagógico:\n\nSIMULADOR DE COSTOS DE "
                                                          "PRODUCCIÓN\nPARA LA CONFECCIÓN DE PRENDAS DE "
                                                          "VESTIR\n\n\nSENA - Centro de Formación en Diseño, "
                                                          "Confección y Moda\n\nColombia - 2023",
                                                     image=self.large_test_image, compound="top",
                                                     font=CTkFont(size=15, weight="bold"))
        self.home_frame_large_image_label.place(relx=0.5, rely=0.5, anchor=CENTER)

        # Productos frame
        self.productos_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.productos_frame.grid_columnconfigure(0, weight=1)
        self.productos_frame_label = CTkLabel(self.productos_frame, text="Productos",
                                              font=CTkFont(size=15, weight="bold"))
        self.productos_frame_label.grid(row=0, column=0, padx=20, pady=10)

        # Contenedor módulo Frame
        self.p_frame = CTkFrame(self.productos_frame, corner_radius=10)
        self.p_frame.grid(row=1, column=0, padx=10, pady=10, columnspan=2, sticky="nsew")

        self.p_frame.rowconfigure(0, weight=1)


        # Add a Treeview widget
        tree_productos = ttk.Treeview(self.p_frame, selectmode="browse", column=("Nombre", "Referencia", "Tiempo Estándar"), show='headings', height=5)
        tree_productos.column("# 1", anchor=CENTER, width=340)
        tree_productos.heading("# 1", text="Nombre")
        tree_productos.column("# 2", anchor=CENTER, width=340)
        tree_productos.heading("# 2", text="Referencia")
        tree_productos.column("# 3", anchor=CENTER, width=340)
        tree_productos.heading("# 3", text="Tiempo Estándar")

        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_producto"
            cursor.execute(sql)
            registro = cursor.fetchall()
            # Insert the data in Treeview widget
            print(registro)
            for r in registro:
                tree_productos.insert('', 'end', text="1", values=(r["nombre"], r["referencia"], r["tiempo_estandar"]))
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
        finally:
            self.conexion.close()

        tree_productos.grid(row=0, column=0, padx=10, pady=10, sticky="nsew")


        # Constructing vertical scrollbar
        # with treeview
        verscrlbar = ttk.Scrollbar(self.p_frame,
                                   orient="vertical",
                                   command=tree_productos.yview)

        # Calling pack method w.r.to vertical
        # scrollbar
        # verscrlbar.pack()
        verscrlbar.grid(row=0, column=1, padx=10, pady=10, sticky="nsew")

        # Configuring treeview
        tree_productos.configure(xscrollcommand=verscrlbar.set)


        self.p_frame_button1 = CTkButton(self.productos_frame, text="Nuevo Producto",  command=self.guardar_carga_prestacional_event)
        self.p_frame_button1.grid(row=2, column=0, pady=10)

        # Materiales frame
        self.materiales_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.materiales_frame.grid_columnconfigure(0, weight=1)
        self.materiales_frame_label = CTkLabel(self.materiales_frame, text="Materiales",
                                              font=CTkFont(size=15, weight="bold"))
        self.materiales_frame_label.grid(row=0, column=0, padx=20, pady=10)

        # Add a Treeview widget
        tree_materiales = ttk.Treeview(self.materiales_frame, column=("Nombre Tipo", "Proveedor", "Unidad de Medida", "Costo Unitario"), show='headings',
                            height=5)
        tree_materiales.column("# 1", anchor=CENTER)
        tree_materiales.heading("# 1", text="Nombre Tipo")
        tree_materiales.column("# 2", anchor=CENTER)
        tree_materiales.heading("# 2", text="Proveedor")
        tree_materiales.column("# 3", anchor=CENTER)
        tree_materiales.heading("# 3", text="Unidad de Medida")
        tree_materiales.column("# 4", anchor=CENTER)
        tree_materiales.heading("# 4", text="Costo Unitario")

        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_material"
            cursor.execute(sql)
            registro = cursor.fetchall()
            # Insert the data in Treeview widget
            print(registro)
            for r in registro:
                tree_materiales.insert('', 'end', text="1", values=(r["nombre_tipo"], r["proveedor"], r["unidad_med"], r["costo_unitario"]))
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
        finally:
            self.conexion.close()

        tree_materiales.grid(row=1, column=0, padx=20, pady=10)

        # create third frame
        self.third_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.third_frame.grid_columnconfigure(0, weight=1)
        self.third_frame_label = CTkLabel(self.third_frame, text="Hoja de Costos",
                                               font=CTkFont(size=15, weight="bold"))
        self.third_frame_label.grid(row=0, column=0, padx=20, pady=10)

        # Carga Prestacional frame
        self.carga_prestacional_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.carga_prestacional_frame.grid_columnconfigure(0, weight=1)
        self.carga_prestacional_frame_label = CTkLabel(self.carga_prestacional_frame, text="Carga Prestacional",
                                          font=CTkFont(size=15, weight="bold"))
        self.carga_prestacional_frame_label.grid(row=0, column=0, pady=10)

        # Contenedor módulo Frame
        self.cp_frame = CTkFrame(self.carga_prestacional_frame, corner_radius=10)
        self.cp_frame.grid(row=1, column=0, padx=10, pady=10, columnspan=2, sticky="nsew")

        self.cp_frame.columnconfigure(0, weight=1)
        self.cp_frame.columnconfigure(1, weight=1)
        self.cp_frame.rowconfigure(0, weight=1)
        self.cp_frame.rowconfigure(1, weight=1)

        self.frame_col1 = CTkFrame(self.cp_frame)
        self.frame_col1.grid(row=0, column=0, sticky="e", padx=10, pady=10)

        self.frame_col2 = CTkFrame(self.cp_frame)
        self.frame_col2.grid(row=0, column=1, sticky="w", padx=10, pady=10)

        self.frame_row1 = CTkFrame(self.cp_frame)
        self.frame_row1.grid(row=1, column=0, sticky="n", columnspan=2, padx=10, pady=10)

        # ============ frame_col1 ============

        self.frame_col1_label1 = CTkLabel(self.frame_col1, text="Salario mímino", fg_color="transparent")
        self.frame_col1_label1.grid(row=0, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry1 = CTkEntry(self.frame_col1, placeholder_text="salario_min", justify="right")
        self.frame_col1_entry1.grid(row=0, column=1, padx=10, pady=10)

        self.frame_col1_label3 = CTkLabel(self.frame_col1, text="Prima de Servicios", fg_color="transparent")
        self.frame_col1_label3.grid(row=1, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry3 = CTkEntry(self.frame_col1, placeholder_text="prima_servicios", justify="right")
        self.frame_col1_entry3.grid(row=1, column=1, padx=10, pady=10)

        self.frame_col1_label5 = CTkLabel(self.frame_col1, text="Intereses de Cesantías", fg_color="transparent")
        self.frame_col1_label5.grid(row=2, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry5 = CTkEntry(self.frame_col1, placeholder_text="intereses_cesantias", justify="right")
        self.frame_col1_entry5.grid(row=2, column=1, padx=10, pady=10)

        self.frame_col1_label7 = CTkLabel(self.frame_col1, text="Caja de Compensación", fg_color="transparent")
        self.frame_col1_label7.grid(row=3, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry7 = CTkEntry(self.frame_col1, placeholder_text="caja_compensacion", justify="right")
        self.frame_col1_entry7.grid(row=3, column=1, padx=10, pady=10)

        self.frame_col1_label9 = CTkLabel(self.frame_col1, text="Arl", fg_color="transparent")
        self.frame_col1_label9.grid(row=4, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry9 = CTkEntry(self.frame_col1, placeholder_text="arl", justify="right")
        self.frame_col1_entry9.grid(row=4, column=1, padx=10, pady=10)

        # ============ frame_col2 ============

        self.frame_col2_label2 = CTkLabel(self.frame_col2, text="Auxilio de Transporte", fg_color="transparent")
        self.frame_col2_label2.grid(row=0, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry2 = CTkEntry(self.frame_col2, placeholder_text="aux_transporte", justify="right")
        self.frame_col2_entry2.grid(row=0, column=1, padx=10, pady=10)

        self.frame_col2_label4 = CTkLabel(self.frame_col2, text="Auxilio de Cesantías", fg_color="transparent")
        self.frame_col2_label4.grid(row=1, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry4 = CTkEntry(self.frame_col2, placeholder_text="aux_cesantias", justify="right")
        self.frame_col2_entry4.grid(row=1, column=1, padx=10, pady=10)

        self.frame_col2_label6 = CTkLabel(self.frame_col2, text="Vacaciones", fg_color="transparent")
        self.frame_col2_label6.grid(row=2, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry6 = CTkEntry(self.frame_col2, placeholder_text="vacaciones", justify="right")
        self.frame_col2_entry6.grid(row=2, column=1, padx=10, pady=10)

        self.frame_col2_label8 = CTkLabel(self.frame_col2, text="Pensión", fg_color="transparent")
        self.frame_col2_label8.grid(row=3, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry8 = CTkEntry(self.frame_col2, placeholder_text="pension", justify="right")
        self.frame_col2_entry8.grid(row=3, column=1, padx=10, pady=10)

        self.frame_col2_label10 = CTkLabel(self.frame_col2, text="Salud", fg_color="transparent")
        self.frame_col2_label10.grid(row=4, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry10 = CTkEntry(self.frame_col2, placeholder_text="salud", justify="right")
        self.frame_col2_entry10.grid(row=4, column=1, padx=10, pady=10)

        # ============ Zona Botones ============

        self.frame_row1_label11 = CTkLabel(self.frame_row1, text="Total Costo Mensual Trabajador", fg_color="transparent")
        self.frame_row1_label11.grid(row=0, column=0, padx=10, pady=10, sticky="w")
        self.frame_row1_entry11 = CTkEntry(self.frame_row1, placeholder_text="total_costo_mensual_trabajador", justify="right")
        self.frame_row1_entry11.grid(row=0, column=1, padx=10, pady=10)

        self.b1utton = CTkButton(self.carga_prestacional_frame, text="Guardar", command=self.guardar_carga_prestacional_event)
        self.b1utton.grid(row=2, column=0, pady=10)

        # create fifth frame
        self.fifth_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.fifth_frame.grid_columnconfigure(0, weight=1)
        self.fifth_frame_label = CTkLabel(self.fifth_frame, text="Valor Minuto",
                                                       font=CTkFont(size=15, weight="bold"))
        self.fifth_frame_label.grid(row=0, column=0, padx=20, pady=10)

        # select default frame
        self.select_frame_by_name("home")

    def abrir_conexion(self):
        self.conexion = sqlite3.connect(BASE_DIR/"scp.db")
        return self.conexion

    def configuraciones_iniciales(self):
        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_config where param='tema'"
            cursor.execute(sql)
            registro = cursor.fetchone()
            # print(registro["value1"])
            self.change_appearance_mode_event(registro["value1"])
            # messagebox.showinfo(title="SCP", message="Conexión establecida!")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
        finally:
            self.conexion.close()

    def select_frame_by_name(self, name):
        # set button color for selected button
        self.home_button.configure(fg_color=("gray75", "gray25") if name == "home" else "transparent")
        self.productos_menu.configure(fg_color=("gray75", "gray25") if name == "productos" else "transparent")
        self.materiales_menu.configure(fg_color=("gray75", "gray25") if name == "materiales" else "transparent")
        self.frame_3_button.configure(fg_color=("gray75", "gray25") if name == "frame_3" else "transparent")
        self.carga_prestacional_menu.configure(fg_color=("gray75", "gray25") if name == "carga" else "transparent")
        self.frame_5_button.configure(fg_color=("gray75", "gray25") if name == "frame_5" else "transparent")

        # show selected frame
        if name == "home":
            self.home_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.home_frame.grid_forget()

        if name == "productos":
            self.productos_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.productos_frame.grid_forget()

        if name == "materiales":
            self.materiales_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.materiales_frame.grid_forget()

        if name == "frame_3":
            self.third_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.third_frame.grid_forget()

        if name == "carga":
            self.carga_prestacional_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.carga_prestacional_frame.grid_forget()

        if name == "frame_5":
            self.fifth_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.fifth_frame.grid_forget()

    def home_button_event(self):
        self.select_frame_by_name("home")

    def productos_menu_event(self):
        self.select_frame_by_name("productos")

    def materiales_menu_event(self):
        self.select_frame_by_name("materiales")

    def frame_3_button_event(self):
        self.select_frame_by_name("frame_3")

    def carga_prestacional_event(self):
        # Habilitar los campos de sólo lectura para cálculos y luego volver a deshabilitarlos en cada caso
        self.frame_col1_entry1.configure(fg_color=('white', 'white'), text_color=('black', 'black'))
        self.frame_col2_entry2.configure(fg_color=('white', 'white'), text_color=('black', 'black'))

        self.frame_col1_entry3.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col1_entry5.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col1_entry7.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col1_entry9.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry4.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry6.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry8.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry10.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))

        self.frame_row1_entry11.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))

        try:
            import locale
            locale.setlocale(locale.LC_ALL, 'es_CO.utf8')  # COLOMBIA

            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_config"
            cursor.execute(sql)
            datos = cursor.fetchall()

            for i in datos:
                if i["param"] == "salario_min":
                    self.salario_min = float(i["value1"])
                    self.frame_col1_entry1.delete(0, END)
                    self.frame_col1_entry1.insert(0, locale.format_string("%.2f", self.salario_min, grouping=True))
                elif i["param"] == "aux_transporte":
                    self.aux_transporte = float(i["value1"])
                    self.frame_col2_entry2.delete(0, END)
                    self.frame_col2_entry2.insert(0, locale.format_string("%.2f", self.aux_transporte, grouping=True))
                elif i["param"] == "prima_servicios":
                    self.prima_servicios = self.salario_min * (float(i["value2"])/100)
                    self.frame_col1_entry3.delete(0, END)
                    self.frame_col1_entry3.insert(0, locale.format_string("%.2f", self.prima_servicios, grouping=True))
                    self.frame_col1_entry3.configure(state='readonly')
                elif i["param"] == "aux_cesantias":
                    self.aux_cesantias = self.salario_min * (float(i["value2"])/100)
                    self.frame_col2_entry4.delete(0, END)
                    self.frame_col2_entry4.insert(0, locale.format_string("%.2f", self.aux_cesantias, grouping=True))
                    self.frame_col2_entry4.configure(state='readonly')
                elif i["param"] == "intereses_cesantias":
                    self.intereses_cesantias = self.salario_min * (float(i["value2"])/100)
                    self.frame_col1_entry5.delete(0, END)
                    self.frame_col1_entry5.insert(0, locale.format_string("%.2f", self.intereses_cesantias, grouping=True))
                    self.frame_col1_entry5.configure(state='readonly')
                elif i["param"] == "vacaciones":
                    self.vacaciones = self.salario_min * (float(i["value2"])/100)
                    self.frame_col2_entry6.delete(0, END)
                    self.frame_col2_entry6.insert(0, locale.format_string("%.2f", self.vacaciones, grouping=True))
                    self.frame_col2_entry6.configure(state='readonly')
                elif i["param"] == "caja_compensacion":
                    self.caja_compensacion = self.salario_min * (float(i["value2"])/100)
                    self.frame_col1_entry7.delete(0, END)
                    self.frame_col1_entry7.insert(0, locale.format_string("%.2f", self.caja_compensacion, grouping=True))
                    self.frame_col1_entry7.configure(state='readonly')
                elif i["param"] == "pension":
                    self.pension = self.salario_min * (float(i["value2"])/100)
                    self.frame_col2_entry8.delete(0, END)
                    self.frame_col2_entry8.insert(0, locale.format_string("%.2f", self.pension, grouping=True))
                    self.frame_col2_entry8.configure(state='readonly')
                elif i["param"] == "arl":
                    self.arl = self.salario_min * (float(i["value2"])/100)
                    self.frame_col1_entry9.delete(0, END)
                    self.frame_col1_entry9.insert(0, locale.format_string("%.2f", self.arl, grouping=True))
                    self.frame_col1_entry9.configure(state='readonly')
                elif i["param"] == "salud":
                    self.salud = self.salario_min * (float(i["value2"])/100)
                    self.frame_col2_entry10.delete(0, END)
                    self.frame_col2_entry10.insert(0, locale.format_string("%.2f", self.salud, grouping=True))
                    self.frame_col2_entry10.configure(state='readonly')
                elif i["param"] == "total_costo_mensual_trabajador":
                    self.total_costo_mensual_trabajador = (self.salario_min * (float(i["value2"])/100)) - self.salud + self.aux_transporte + self.salario_min
                    self.frame_row1_entry11.delete(0, END)
                    self.frame_row1_entry11.insert(0, locale.format_string("%.2f", self.total_costo_mensual_trabajador, grouping=True))
                    self.frame_row1_entry11.configure(state='readonly')

        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error recuperando los datos.\n{e}")
        finally:
            self.conexion.close()

        self.select_frame_by_name("carga")

    def carga_prestacional_event_vieja(self):
        try:
            import locale
            locale.setlocale(locale.LC_ALL, 'es_CO.utf8')  # COLOMBIA

            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_config"
            cursor.execute(sql)
            datos = cursor.fetchall()

            for i in datos:
                if i["param"] == "salario_min":
                    self.frame_col1_entry1.delete(0, END)
                    self.frame_col1_entry1.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "aux_transporte":
                    self.frame_col2_entry2.delete(0, END)
                    self.frame_col2_entry2.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "prima_servicios":
                    self.frame_col1_entry3.delete(0, END)
                    self.frame_col1_entry3.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "aux_cesantias":
                    self.frame_col2_entry4.delete(0, END)
                    self.frame_col2_entry4.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "intereses_cesantias":
                    self.frame_col1_entry5.delete(0, END)
                    self.frame_col1_entry5.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "vacaciones":
                    self.frame_col2_entry6.delete(0, END)
                    self.frame_col2_entry6.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "caja_compensacion":
                    self.frame_col1_entry7.delete(0, END)
                    self.frame_col1_entry7.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "pension":
                    self.frame_col2_entry8.delete(0, END)
                    self.frame_col2_entry8.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "arl":
                    self.frame_col1_entry9.delete(0, END)
                    self.frame_col1_entry9.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "salud":
                    self.frame_col2_entry10.delete(0, END)
                    self.frame_col2_entry10.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "total_costo_mensual_trabajador":
                    self.frame_row1_entry11.delete(0, END)
                    self.frame_row1_entry11.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))

        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error recuperando los datos.\n{e}")
        finally:
            self.conexion.close()

        self.select_frame_by_name("carga")

    def guardar_carga_prestacional_event(self):
        records_to_update = [
            (self.frame_col1_entry1.get().replace(".", "").replace(",", "."), "salario_min"),
            (self.frame_col2_entry2.get().replace(".", "").replace(",", "."), "aux_transporte")
        ]

        try:
            self.abrir_conexion()
            cursor = self.conexion.cursor()
            sql = "update scp_config set value1 = ? where param = ?"
            cursor.executemany(sql, records_to_update)
            self.conexion.commit()
            self.conexion.close()
            # Actualizar resto de datos después de guardar el salario mínimo y auxilio de transporte
            self.carga_prestacional_event()
            records_to_update = [
                (self.frame_col1_entry3.get().replace(".", "").replace(",", "."), "prima_servicios"),
                (self.frame_col2_entry4.get().replace(".", "").replace(",", "."), "aux_cesantias"),
                (self.frame_col1_entry5.get().replace(".", "").replace(",", "."), "intereses_cesantias"),
                (self.frame_col2_entry6.get().replace(".", "").replace(",", "."), "vacaciones"),
                (self.frame_col1_entry7.get().replace(".", "").replace(",", "."), "caja_compensacion"),
                (self.frame_col2_entry8.get().replace(".", "").replace(",", "."), "pension"),
                (self.frame_col1_entry9.get().replace(".", "").replace(",", "."), "arl"),
                (self.frame_col2_entry10.get().replace(".", "").replace(",", "."), "salud"),
                (self.frame_row1_entry11.get().replace(".", "").replace(",", "."), "total_costo_mensual_trabajador")
            ]
            self.abrir_conexion()
            cursor = self.conexion.cursor()
            sql = "update scp_config set value1 = ? where param = ?"
            cursor.executemany(sql, records_to_update)
            self.conexion.commit()
            messagebox.showinfo(title="SCP", message="Carga prestacional guardada!")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error guardando en base de datos.\n{e}")
        finally:
            self.conexion.close()

    def frame_5_button_event(self):
        self.select_frame_by_name("frame_5")

    def change_appearance_mode_event(self, modo=False):
        if modo:
            # Modificamos la apariencia desde la base de datos
            set_appearance_mode(modo)
            # Cambiamos valor a varible <class 'tkinter.StringVar'>
            self.selected_option.set(modo)
        else:
            # Modificamos la apariencia desde el menú de configuración
            set_appearance_mode(self.selected_option.get())
            # Guardamos cambios en DB
            try:
                self.abrir_conexion()
                cursor = self.conexion.cursor()
                sql = "update scp_config set value1 = ? where param = 'tema'"
                datos = (self.selected_option.get(),)
                cursor.execute(sql, datos)
                self.conexion.commit()
                # messagebox.showinfo(title="SCP", message="Configuración guardada!")
            except Exception as e:
                messagebox.showerror(title="SCP", message=f"Error guardando en base de datos.\n{e}")
            finally:
                self.conexion.close()

    def help_menu_event(self):
        # Crear una ventana secundaria.
        ventana_secundaria = CTkToplevel()

        # icon
        ventana_secundaria.wm_iconbitmap()
        ventana_secundaria.after(300, lambda: ventana_secundaria.iconphoto(False, self.iconpath))
        # fin - icon

        ventana_secundaria.resizable(False, False)
        # quitar minimizar....
        ventana_secundaria.title("Ventana secundaria")
        ventana_secundaria.config(width=300, height=200)
        # Crear un botón dentro de la ventana secundaria
        # para cerrar la misma.
        boton_cerrar = CTkButton(
            ventana_secundaria,
            text="Cerrar ventana",
            command=ventana_secundaria.destroy
        )
        boton_cerrar.place(x=75, y=75)
        ventana_secundaria.focus()
        # Modal...
        ventana_secundaria.transient(self)  # dialog window is related to main
        ventana_secundaria.wait_visibility()
        ventana_secundaria.grab_set()
        ventana_secundaria.wait_window()


    def help_menu_about_event(self):
        messagebox.showinfo("Acerca de ...", "\nProducto Técnico-Pedagógico:\n\nSIMULADOR DE COSTOS DE PRODUCCIÓN "
                                             "PARA LA CONFECCIÓN DE PRENDAS DE VESTIR\n\n\nSENA - Centro de Formación "
                                             "en Diseño, Confección y Moda\n\nColombia - 2023")


if __name__ == "__main__":
    set_appearance_mode("system")  # light | dark | system
    set_default_color_theme(BASE_DIR/"purple.json")
    # set_default_color_theme("green")

    app = App()

    # app.iconbitmap(BASE_DIR/"favicon_io/favicon.ico")
    # app.wm_iconbitmap(BASE_DIR/"favicon_io/favicon_windows.ico")

    # app.after(201, lambda: app.iconbitmap(BASE_DIR/"favicon_io/favicon_windows.ico"))

    # icono_chico = PhotoImage(file=BASE_DIR/"favicon_io/favicon-16x16.png")
    # icono_grande = PhotoImage(file=BASE_DIR/"favicon_io/favicon-32x32.png")
    # app.iconphoto(False, icono_grande, icono_chico)

    import ctypes

    myappid = 'net.tikole.scp.1'  # arbitrary string
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

    app.title("Simulador de Costos de Producción")
    app.resizable(True, True)

    app.state('normal')

    app.configuraciones_iniciales()

    app.mainloop()
