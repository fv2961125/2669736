import os
from customtkinter import *
from tkinter import messagebox, PhotoImage
import tkinter as tk
from tkinter import ttk
import sqlite3
from PIL import Image
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
# Path(__file__).resolve().parent

BASE_DIR = os.path.abspath('.')

print(f"Ruta Proyecto: {BASE_DIR}")

class App(CTk):
    def __init__(self):
        super().__init__()

        self.geometry("1530x670+0+0")

        self.conexion = False
        #icon
        self.iconpath = PhotoImage(file=f"{BASE_DIR}/favicon_io/logo32.png")
        self.wm_iconbitmap()
        self.iconphoto(False, self.iconpath)

        # menu de plataforma macOS
        plattform = tk.Menu(self)

        self.selected_option = tk.StringVar()
        self.hoja_costos_producto = tk.StringVar()
        # fin - menu

        # menu
        # menubar = tk.Menu(self)

        file_menu = tk.Menu(plattform, tearoff=0)
        file_menu.add_command(label="Exportar datos...")
        file_menu.add_command(label="Importar datos")
        file_menu.add_separator()
        file_menu.add_command(label="Hoja de Costos")
        file_menu.add_separator()
        file_menu.add_command(label="Salir", command=self.quit)

        edit_menu = tk.Menu(plattform, tearoff=0)
        edit_menu.add_command(label="Productos")
        edit_menu.add_command(label="Materiales")

        conf_menu = tk.Menu(plattform, tearoff=0)
        conf_menu.add_radiobutton(label="Tema Claro", variable=self.selected_option, value="Light",
                                  command=self.change_appearance_mode_event)
        conf_menu.add_radiobutton(label="Tema Oscuro", variable=self.selected_option, value="Dark",
                                  command=self.change_appearance_mode_event)
        conf_menu.add_radiobutton(label="Tema Sistema", variable=self.selected_option, value="System",
                                  command=self.change_appearance_mode_event)
        conf_menu.add_separator()
        conf_menu.add_command(label="Carga Prestacional", command=self.carga_prestacional_event)

        help_menu = tk.Menu(plattform, tearoff=0)
        help_menu.add_command(label="Ayuda", accelerator="Control+A", command=self.help_menu_event)
        help_menu.add_separator()
        help_menu.add_command(label="Acerca de...", accelerator="Control+X", command=self.help_menu_about_event)
        self.bind_all("<Control-X>", self.help_menu_event)

        plattform.add_cascade(label="Archivo", menu=file_menu)
        plattform.add_cascade(label="Editar", menu=edit_menu)
        plattform.add_cascade(label="Configuración", menu=conf_menu)
        plattform.add_cascade(label="Ayuda", menu=help_menu)

        self.config(menu=plattform)

        # fin - menu

        # set grid layout 1x2
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)

        self.logo_image = CTkImage(Image.open(f"{BASE_DIR}/favicon_io/logo32.png"), size=(32, 32))
        self.large_test_image = CTkImage(Image.open(f"{BASE_DIR}/test_images/logoSena.png"), size=(180, 176))
        self.image_icon_image = CTkImage(Image.open(f"{BASE_DIR}/test_images/image_icon_light.png"), size=(20, 20))
        self.home_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/test_images/home_dark.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/test_images/home_light.png"), size=(20, 20))
        self.chat_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/test_images/chat_dark.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/test_images/chat_light.png"), size=(20, 20))
        self.add_user_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/test_images/add_user_dark.png"),
                                       dark_image=Image.open(f"{BASE_DIR}/test_images/add_user_light.png"),
                                       size=(20, 20))

        # create navigation frame
        self.navigation_frame = CTkFrame(self, corner_radius=10)
        self.navigation_frame.grid(row=0, column=0, sticky="nsew")
        self.navigation_frame.grid_rowconfigure(7, weight=1)

        self.navigation_frame_label = CTkLabel(self.navigation_frame, text="  SIMULADOR\n  Costos de Producción",
                                               image=self.logo_image,
                                               compound="left", font=CTkFont(size=15, weight="bold"))
        self.navigation_frame_label.grid(row=0, column=0, padx=20, pady=20)

        self.home_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                     text="Inicio",
                                     fg_color="transparent", text_color=("gray10", "gray90"),
                                     hover_color=("gray70", "gray30"),
                                     image=self.home_image, anchor="w", command=self.home_button_event)
        self.home_button.grid(row=1, column=0, sticky="ew")

        self.productos_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                        text="Productos",
                                        fg_color="transparent", text_color=("gray10", "gray90"),
                                        hover_color=("gray70", "gray30"),
                                        image=self.chat_image, anchor="w", command=self.productos_menu_event)
        self.productos_menu.grid(row=2, column=0, sticky="ew")

        self.materiales_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                        text="Materiales",
                                        fg_color="transparent", text_color=("gray10", "gray90"),
                                        hover_color=("gray70", "gray30"),
                                        image=self.chat_image, anchor="w", command=self.materiales_menu_event)
        self.materiales_menu.grid(row=3, column=0, sticky="ew")

        self.frame_3_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                        text="Hoja de Costos",
                                        fg_color="transparent", text_color=("gray10", "gray90"),
                                        hover_color=("gray70", "gray30"),
                                        image=self.add_user_image, anchor="w", command=self.frame_3_button_event)
        self.frame_3_button.grid(row=4, column=0, sticky="ew")

        self.carga_prestacional_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                                 text="Carga Prestacional",
                                                 fg_color="transparent", text_color=("gray10", "gray90"),
                                                 hover_color=("gray70", "gray30"),
                                                 image=self.add_user_image, anchor="w", command=self.carga_prestacional_event)
        self.carga_prestacional_menu.grid(row=5, column=0, sticky="ew")
        self.salario_min = 0
        self.aux_transporte = 0
        self.prima_servicios = 0
        self.aux_cesantias = 0
        self.intereses_cesantias = 0
        self.vacaciones = 0
        self.caja_compensacion = 0
        self.pension = 0
        self.arl = 0
        self.salud = 0
        self.total_costo_mensual_trabajador = 0


        self.frame_5_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                        text="Valor minuto",
                                        fg_color="transparent", text_color=("gray10", "gray90"),
                                        hover_color=("gray70", "gray30"),
                                        image=self.add_user_image, anchor="w", command=self.frame_5_button_event)
        self.frame_5_button.grid(row=6, column=0, sticky="ew")

        self.appearance_mode_menu = CTkOptionMenu(self.navigation_frame, values=["Light", "Dark", "System"],
                                                  command=self.change_appearance_mode_event)
        self.appearance_mode_menu.grid(row=7, column=0, padx=20, pady=20, sticky="s")

        # create home frame
        self.home_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.home_frame.grid_columnconfigure(0, weight=1)

        self.home_frame_large_image_label = CTkLabel(self.home_frame,
                                                     text="\nProducto Técnico-Pedagógico:\n\nSIMULADOR DE COSTOS DE "
                                                          "PRODUCCIÓN\nPARA LA CONFECCIÓN DE PRENDAS DE "
                                                          "VESTIR\n\n\nSENA - Centro de Formación en Diseño, "
                                                          "Confección y Moda\n\nColombia - 2023",
                                                     image=self.large_test_image, compound="top",
                                                     font=CTkFont(size=15, weight="bold"))
        self.home_frame_large_image_label.place(relx=0.5, rely=0.5, anchor=CENTER)

        # Productos frame
        self.productos_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.productos_frame.grid_columnconfigure(0, weight=1)
        self.productos_frame_label = CTkLabel(self.productos_frame, text="Productos",
                                              font=CTkFont(size=15, weight="bold"))
        self.productos_frame_label.grid(row=0, column=0, padx=20, pady=10)

        # Contenedor módulo Frame
        self.p_frame = CTkFrame(self.productos_frame, corner_radius=10)
        self.p_frame.grid(row=1, column=0, padx=10, pady=10, columnspan=2, sticky="nsew")

        self.p_frame.rowconfigure(0, weight=1)

        self.style = ttk.Style()
        self.style.configure("mystyle.Treeview.Heading", font=('Calibri', 12, 'bold'))


        # Add a Treeview widget
        self.tree_productos = ttk.Treeview(self.p_frame, style="mystyle.Treeview", selectmode="browse")

        self.tree_productos['columns'] = ('id', 'nombre', 'referencia', 'tiempo_estandar')
        self.tree_productos.column('#0', stretch=NO, minwidth=100, width=0)
        self.tree_productos.column('#1', stretch=NO, minwidth=100, width=200, anchor=CENTER)
        self.tree_productos.column('#2', stretch=NO, minwidth=100, width=200, anchor=CENTER)
        self.tree_productos.column('#3', stretch=NO, minwidth=100, width=200, anchor=CENTER)

        self.tree_productos.heading('id', text='ID', anchor=CENTER)
        self.tree_productos.heading('nombre', text='Nombre', anchor=CENTER)
        self.tree_productos.heading('referencia', text='Referencia', anchor=CENTER)
        self.tree_productos.heading('tiempo_estandar', text='Tiempo Estándar', anchor=CENTER)

        self.tree_productos.bind("<Double-1>", self.OnDoubleClick)
        self.tree_productos.grid(row=0, column=0, columnspan=6, pady=5)
        self.scroll = ttk.Scrollbar(self.p_frame, orient="vertical", command=self.tree_productos.yview)
        self.scroll.grid(row=0, column=6, sticky=NS)
        self.tree_productos.configure(yscrollcommand=self.scroll.set)

        try:
            self.abrir_conexion()
            #self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_producto"
            cursor.execute(sql)
            registro = cursor.fetchall()
            # Insert the data in Treeview widget
            for r in registro:
                # self.tree_productos.insert('', 'end', values=(r["id"], r["nombre"], r["referencia"], r["tiempo_estandar"]))
                self.tree_productos.insert("", END, values=r)
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
        finally:
            self.conexion.close()

        self.tree_productos.grid(row=0, column=0, padx=10, pady=10, sticky="nsew")



        self.p_frame_button1 = CTkButton(self.productos_frame, text="Nuevo Producto",
                                         command=self.guardar_carga_prestacional_event)
        self.p_frame_button1.grid(row=2, column=0, pady=10)

        # Materiales frame
        self.materiales_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.materiales_frame.grid_columnconfigure(0, weight=1)
        self.materiales_frame_label = CTkLabel(self.materiales_frame, text="Materiales",
                                              font=CTkFont(size=15, weight="bold"))
        self.materiales_frame_label.grid(row=0, column=0, padx=20, pady=10)

        # Add a Treeview widget
        tree_materiales = ttk.Treeview(self.materiales_frame, column=("Nombre Tipo", "Proveedor", "Unidad de Medida", "Costo Unitario"), show='headings',
                            height=5)
        tree_materiales.column("# 1", anchor=CENTER)
        tree_materiales.heading("# 1", text="Nombre Tipo")
        tree_materiales.column("# 2", anchor=CENTER)
        tree_materiales.heading("# 2", text="Proveedor")
        tree_materiales.column("# 3", anchor=CENTER)
        tree_materiales.heading("# 3", text="Unidad de Medida")
        tree_materiales.column("# 4", anchor=CENTER)
        tree_materiales.heading("# 4", text="Costo Unitario")

        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_material where tipo='Directo' "
            cursor.execute(sql)
            registro = cursor.fetchall()
            # Insert the data in Treeview widget
            for r in registro:
                tree_materiales.insert('', 'end', text="1",
                                       values=(r["nombre_tipo"], r["proveedor"], r["unidad_med"], r["costo_unitario"]))
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
        finally:
            self.conexion.close()

        tree_materiales.grid(row=1, column=0, padx=20, pady=10)

        # Hoja de Costos
        import traceback
        try:
            # create third frame
            self.third_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
            self.third_frame.grid_columnconfigure(0, weight=1)
            self.third_frame_label = CTkLabel(self.third_frame, text="Hoja de Costos",
                                            font=CTkFont(size=15, weight="bold"))
            self.third_frame_label.grid(row=0, column=0, padx=20, pady=10)

            # Contenedor módulo Frame
            self.hc_frame = CTkScrollableFrame(self.third_frame, corner_radius=10, height=600)
            self.hc_frame.grid(row=1, column=0, padx=10, pady=10, sticky="nsew")

            # create tabview
            self.tabview = CTkTabview(self.hc_frame, width=1240)
            self.tabview.grid(row=1, column=0, padx=20, pady=20, sticky="nsew")

            tab1 = "RESUMEN"
            tab2 = "MATERIALES DIRECTOS"
            tab3 = "MANO DE OBRA DIRECTA"
            tab4 = "INDIRECTOS DE FABRICACIÓN"
            tab5 = "MANO DE OBRA INDIRECTA"
            tab6 = "COSTOS ADMINISTRATIVOS"
            tab7 = "DEPRECIACIÓN MAQUINARIA"

            self.tabview.add(tab1)
            self.tabview.add(tab2)
            self.tabview.add(tab3)
            self.tabview.add(tab4)
            self.tabview.add(tab5)
            self.tabview.add(tab6)
            self.tabview.add(tab7)

            # configure grid of individual tabs

            # tab1
            self.tabview.tab(tab1).grid_columnconfigure(0, weight=1, uniform=True)
            self.tabview.tab(tab1).grid_columnconfigure(1, weight=1, uniform=True)
            self.tabview.tab(tab1).grid_columnconfigure(2, weight=1, uniform=True)
            self.tabview.tab(tab1).grid_columnconfigure(3, weight=1, uniform=True)
            # tab2
            self.tabview.tab(tab2).grid_columnconfigure(0, weight=1, uniform=True)
            # tab3
            self.tabview.tab(tab3).grid_columnconfigure(0, weight=1, uniform=True)
            self.tabview.tab(tab3).grid_columnconfigure(1, weight=1, uniform=True)
            self.tabview.tab(tab3).grid_columnconfigure(2, weight=1, uniform=True)
            self.tabview.tab(tab3).grid_columnconfigure(3, weight=1, uniform=True)
            self.tabview.tab(tab3).grid_columnconfigure(4, weight=1, uniform=True)
            self.tabview.tab(tab3).grid_columnconfigure(5, weight=1, uniform=True)
            # tab4
            self.tabview.tab(tab4).grid_columnconfigure(0, weight=1, uniform=True)
            self.tabview.tab(tab4).grid_columnconfigure(1, weight=1, uniform=True)
            # tab5
            self.tabview.tab(tab5).grid_columnconfigure(0, weight=1, uniform=True)
            self.tabview.tab(tab5).grid_columnconfigure(1, weight=1, uniform=True)
            self.tabview.tab(tab5).grid_columnconfigure(2, weight=1, uniform=True)
            # tab6
            self.tabview.tab(tab6).grid_columnconfigure(0, weight=1, uniform=True)
            self.tabview.tab(tab6).grid_columnconfigure(1, weight=1, uniform=True)
            # tab7
            self.tabview.tab(tab7).grid_columnconfigure(0, weight=1, uniform=True)


            # tab1 ===========================

            productos = ["Seleccione el Producto"]
            self.hoja_costos_producto = StringVar(value="Seleccione el Producto")
            try:
                self.abrir_conexion()
                self.conexion.row_factory = sqlite3.Row
                cursor = self.conexion.cursor()
                sql = "select * from scp_producto"
                cursor.execute(sql)
                registro = cursor.fetchall()
                # Construir listado para combobox
                for r in registro:
                    productos.append(f"{r['id']}.{r['nombre']}")
            except Exception as e:
                messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
            finally:
                self.conexion.close()

            self.tab1_producto = CTkOptionMenu(self.tabview.tab(tab1), dynamic_resizing=True, values=productos,
                                            command=self.optionmenu_callback,
                                            variable=self.hoja_costos_producto)
            # self.hoja_costos_producto = tk.StringVar(value="option 2")

            self.tab1_producto.grid(row=0, column=0, columnspan="4", padx=10, pady=10)

            self.tab1_fecha = CTkLabel(self.tabview.tab(tab1), text="FECHA:")
            self.tab1_fecha.grid(row=1, column=0, padx=20, pady=20)
            self.tab1_fecha_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
            self.tab1_fecha_valor.grid(row=1, column=1, padx=20, pady=20)

            self.tab1_ref = CTkLabel(self.tabview.tab(tab1), text="REF:")
            self.tab1_ref.grid(row=1, column=2, padx=20, pady=20)
            self.tab1_ref_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
            self.tab1_ref_valor.grid(row=1, column=3, padx=20, pady=20)

            self.tab1_tiempoE = CTkLabel(self.tabview.tab(tab1), text="Tiempo Estándar:")
            self.tab1_tiempoE.grid(row=2, column=0, padx=20, pady=20)
            self.tab1_tiempoE_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
            self.tab1_tiempoE_valor.grid(row=2, column=1, padx=20, pady=20)

            self.tab1_eficiencia = CTkLabel(self.tabview.tab(tab1), text="Eficiencia:")
            self.tab1_eficiencia.grid(row=2, column=2, padx=20, pady=20)
            self.tab1_eficiencia_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
            self.tab1_eficiencia_valor.grid(row=2, column=3, padx=20, pady=20)

            self.tab1_jornadaL = CTkLabel(self.tabview.tab(tab1), text="Jornada Laboral:")
            self.tab1_jornadaL.grid(row=3, column=0, padx=20, pady=20)
            self.tab1_jornadaL_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
            self.tab1_jornadaL_valor.grid(row=3, column=1, padx=20, pady=20)

            self.tab1_num_operarios = CTkLabel(self.tabview.tab(tab1), text="N° Operarios:")
            self.tab1_num_operarios.grid(row=3, column=2, padx=20, pady=20)
            self.tab1_num_operarios_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
            self.tab1_num_operarios_valor.grid(row=3, column=3, padx=20, pady=20)

            self.tab1_min_mes = CTkLabel(self.tabview.tab(tab1), text="Minutos/Mes:")
            self.tab1_min_mes.grid(row=4, column=0, padx=20, pady=20)
            self.tab1_min_mes_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
            self.tab1_min_mes_valor.grid(row=4, column=1, padx=20, pady=20)

            self.tab1_utilidad = CTkLabel(self.tabview.tab(tab1), text="UTILIDAD (%):")
            self.tab1_utilidad.grid(row=4, column=2, padx=20, pady=20)
            self.tab1_utilidad_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
            self.tab1_utilidad_valor.grid(row=4, column=3, padx=20, pady=20)

            self.tab1_u_prod_mes = CTkLabel(self.tabview.tab(tab1), text="*** Unid a Prod/mes: ****")
            self.tab1_u_prod_mes.grid(row=5, column=0, padx=20, pady=20)
            self.tab1_u_prod_mes_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#6acee2", width=200)
            self.tab1_u_prod_mes_valor.grid(row=5, column=1, padx=20, pady=20)

            self.tab1_precio_venta = CTkLabel(self.tabview.tab(tab1), text="*** PRECIO DE VENTA: ****")
            self.tab1_precio_venta.grid(row=5, column=2, padx=20, pady=20)
            self.tab1_precio_venta_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#6acee2", width=200)
            self.tab1_precio_venta_valor.grid(row=5, column=3, padx=20, pady=20)
            # sección tab1
            self.tab1_resumen_costo = CTkLabel(self.tabview.tab(tab1), text="== RESUMEN DEL COSTO ==")
            self.tab1_resumen_costo.grid(row=6, column=0, columnspan="4", padx=20, pady=20)
            # fin sección

            self.tab1_mat_directo = CTkLabel(self.tabview.tab(tab1), text="*** MATERIALES.DIRECTOS: ****")
            self.tab1_mat_directo.grid(row=7, column=0, padx=20, pady=20)
            self.tab1_mat_directo_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#6acee2", width=200)
            self.tab1_mat_directo_valor.grid(row=7, column=1, padx=20, pady=20)

            self.tab1_MOD = CTkLabel(self.tabview.tab(tab1), text="*** M.O.D: ****")
            self.tab1_MOD.grid(row=7, column=2, padx=20, pady=20)
            self.tab1_MOD_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#6acee2", width=200)
            self.tab1_MOD_valor.grid(row=7, column=3, padx=20, pady=20)

            self.tab1_CIF = CTkLabel(self.tabview.tab(tab1), text="*** CIF: ****")
            self.tab1_CIF.grid(row=8, column=0, padx=20, pady=20)
            self.tab1_CIF_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#6acee2", width=200)
            self.tab1_CIF_valor.grid(row=8, column=1, padx=20, pady=20)
            self.tab1_CIF_total_acumulado = 0

            self.tab1_COSTO_TOTAL = CTkLabel(self.tabview.tab(tab1), text="*** COSTO TOTAL: ****")
            self.tab1_COSTO_TOTAL.grid(row=8, column=2, padx=20, pady=20)
            self.tab1_COSTO_TOTAL_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#6acee2", width=200)
            self.tab1_COSTO_TOTAL_valor.grid(row=8, column=3, padx=20, pady=20)

            # tab2 ===========================

            self.tab2_titul1 = CTkLabel(self.tabview.tab(tab2), text="Producto:")
            self.tab2_titul1.grid(row=0, column=0, padx=20, pady=20)

            # Add a Treeview widget
            self.tree_materiales_directos = ttk.Treeview(self.tabview.tab(tab2), style="mystyle.Treeview", selectmode="extended")

            self.tree_materiales_directos['columns'] = ('id', 'nombre_tipo', 'proveedor', 'unidad_med', 'consumo_unit', 'costo_unitario', 'costo_total')
            self.tree_materiales_directos.column('#0', stretch=NO, width=0, anchor=CENTER)
            self.tree_materiales_directos.column('#1', stretch=NO, minwidth=100, width=50, anchor=CENTER)
            self.tree_materiales_directos.column('#2', stretch=NO, minwidth=100, width=200, anchor=CENTER)
            self.tree_materiales_directos.column('#3', stretch=NO, minwidth=100, width=150, anchor=CENTER)
            self.tree_materiales_directos.column('#4', stretch=NO, minwidth=100, width=200, anchor=CENTER)
            self.tree_materiales_directos.column('#5', stretch=NO, minwidth=100, width=200, anchor="e")
            self.tree_materiales_directos.column('#6', stretch=NO, minwidth=100, width=200, anchor="e")
            self.tree_materiales_directos.column('#7', stretch=NO, minwidth=100, width=200, anchor="e")

            self.tree_materiales_directos.heading('id', text='ID', anchor=CENTER)
            self.tree_materiales_directos.heading('nombre_tipo', text='TIPO DE MATERIAL', anchor=CENTER)
            self.tree_materiales_directos.heading('proveedor', text='PROVEEDOR', anchor=CENTER)
            self.tree_materiales_directos.heading('unidad_med', text='UNIDAD MEDIDA', anchor=CENTER)
            self.tree_materiales_directos.heading('consumo_unit', text='CONSUMO UNIT', anchor=CENTER)
            self.tree_materiales_directos.heading('costo_unitario', text='COSTO UNITARIO', anchor=CENTER)
            self.tree_materiales_directos.heading('costo_total', text='COSTO TOTAL', anchor=CENTER)
            self.tree_materiales_directos.grid(row=1, columnspan=6, padx=20, pady=20)

            self.tab2_total = CTkLabel(self.tabview.tab(tab2), text="TOTALES:", fg_color="#6acee2", width=200)
            self.tab2_total.grid(row=2, column=0, padx=20, pady=20)

            # tab3 ===========================

            self.tab3_titul1 = CTkLabel(self.tabview.tab(tab3), text="COSTO MANO DE OBRA DIRECTA")
            self.tab3_titul1.grid(row=0, column=0, columnspan="6", padx=10, pady=10)

            self.tab3_sal_basico = CTkLabel(self.tabview.tab(tab3), text="SAL BASICO:")
            self.tab3_sal_basico.grid(row=1, column=0, padx=20, pady=20)
            self.tab3_sal_basico_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
            self.tab3_sal_basico_valor.grid(row=1, column=1, padx=20, pady=20)

            self.tab3_carga_pres = CTkLabel(self.tabview.tab(tab3), text="CARGA PREST:")
            self.tab3_carga_pres.grid(row=1, column=2, padx=20, pady=20)
            self.tab3_carga_pres_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
            self.tab3_carga_pres_valor.grid(row=1, column=3, padx=20, pady=20)

            self.tab3_total_sal = CTkLabel(self.tabview.tab(tab3), text="TOTAL SALARIO:")
            self.tab3_total_sal.grid(row=1, column=4, padx=20, pady=20)
            self.tab3_total_sal_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
            self.tab3_total_sal_valor.grid(row=1, column=5, padx=20, pady=20)

            self.tab3_sub_transp = CTkLabel(self.tabview.tab(tab3), text="SUB TRANSP:")
            self.tab3_sub_transp.grid(row=2, column=0, padx=20, pady=20)
            self.tab3_sub_transp_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
            self.tab3_sub_transp_valor.grid(row=2, column=1, padx=20, pady=20)

            self.tab3_otros = CTkLabel(self.tabview.tab(tab3), text="OTROS:")
            self.tab3_otros.grid(row=2, column=2, padx=20, pady=20)
            self.tab3_otros_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#b7de78", width=150)
            self.tab3_otros_valor.grid(row=2, column=3, padx=20, pady=20)

            self.tab3_total_sal_otros = CTkLabel(self.tabview.tab(tab3), text="TOTAL SALARIO:")
            self.tab3_total_sal_otros.grid(row=2, column=4, padx=20, pady=20)
            self.tab3_total_sal_otros_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#b7de78", width=150)
            self.tab3_total_sal_otros_valor.grid(row=2, column=5, padx=20, pady=20)

            self.tab3_costo_mod = CTkLabel(self.tabview.tab(tab3), text="COSTO TOTAL M.O.D:")
            self.tab3_costo_mod.grid(row=3, column=0, columnspan="2",  padx=20, pady=20)
            self.tab3_costo_mod_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
            self.tab3_costo_mod_valor.grid(row=3, column=2, padx=20, pady=20)

            self.tab3_min_mes = CTkLabel(self.tabview.tab(tab3), text="MINUTOS EFECTIVOS/MES:")
            self.tab3_min_mes.grid(row=3, column=3, columnspan="2", padx=20, pady=20)
            self.tab3_min_mes_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
            self.tab3_min_mes_valor.grid(row=3, column=5, padx=20, pady=20)

            self.tab3_fact_min = CTkLabel(self.tabview.tab(tab3), text="FACTOR MINUTO:")
            self.tab3_fact_min.grid(row=4, column=0, columnspan="2",  padx=20, pady=20)
            self.tab3_fact_min_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
            self.tab3_fact_min_valor.grid(row=4, column=2, padx=20, pady=20)

            self.tab3_unitario_mod = CTkLabel(self.tabview.tab(tab3), text="COSTO TOTAL UNITARIO M.O.D:")
            self.tab3_unitario_mod.grid(row=4, column=3, columnspan="2", padx=20, pady=20)
            self.tab3_unitario_mod_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#6acee2", width=150)
            self.tab3_unitario_mod_valor.grid(row=4, column=5, padx=20, pady=20)

            # tab4 ===========================

            self.tab4_titul1 = CTkLabel(self.tabview.tab(tab4), text="COSTOS INDIRECTOS DE FABRICACION\nCOSTO MATERIALES INDIRECTOS:")
            self.tab4_titul1.grid(row=0, column=0, columnspan="6", padx=20, pady=20)

            # Add a Treeview widget
            self.tree_materiales_indirectos = ttk.Treeview(self.tabview.tab(tab4), style="mystyle.Treeview", selectmode="extended")

            self.tree_materiales_indirectos['columns'] = ('id', 'nombre_tipo', 'unidad_med', 'consumo_mes', 'costo_unitario', 'costo_total', 'costo_asignado')
            self.tree_materiales_indirectos.column('#0', stretch=NO, width=0, anchor=CENTER)
            self.tree_materiales_indirectos.column('#1', stretch=NO, minwidth=100, width=50, anchor=CENTER)
            self.tree_materiales_indirectos.column('#2', stretch=NO, minwidth=100, width=200, anchor=CENTER)
            self.tree_materiales_indirectos.column('#3', stretch=NO, minwidth=100, width=100, anchor=CENTER)
            self.tree_materiales_indirectos.column('#4', stretch=NO, minwidth=100, width=150, anchor=CENTER)
            self.tree_materiales_indirectos.column('#5', stretch=NO, minwidth=100, width=200, anchor="e")
            self.tree_materiales_indirectos.column('#6', stretch=NO, minwidth=100, width=200, anchor="e")
            self.tree_materiales_indirectos.column('#7', stretch=NO, minwidth=100, width=200, anchor="e")

            self.tree_materiales_indirectos.heading('id', text='ID', anchor=CENTER)
            self.tree_materiales_indirectos.heading('nombre_tipo', text='TIPO DE MATERIAL', anchor=CENTER)
            self.tree_materiales_indirectos.heading('unidad_med', text='UNIDAD MEDIDA', anchor=CENTER)
            self.tree_materiales_indirectos.heading('consumo_mes', text='CONSUMO/MES', anchor=CENTER)
            self.tree_materiales_indirectos.heading('costo_unitario', text='COSTO', anchor=CENTER)
            self.tree_materiales_indirectos.heading('costo_total', text='COSTO TOTAL', anchor=CENTER)
            self.tree_materiales_indirectos.heading('costo_asignado', text='COSTO ASIGNADO', anchor=CENTER)
            self.tree_materiales_indirectos.grid(row=1, column=0,  columnspan=6, padx=20, pady=20)

            self.tab4_total1 = CTkLabel(self.tabview.tab(tab4), text="TOTALES:", fg_color="#6acee2", width=300)
            self.tab4_total1.grid(row=2, column=0, columnspan="2", padx=20, pady=20)
            self.tab4_total2 = CTkLabel(self.tabview.tab(tab4), text="TOTALES ASIGNADO:", fg_color="#6acee2", width=400)
            self.tab4_total2.grid(row=2, column=2, columnspan="3", padx=20, pady=20)

            # tab5 ===========================

            self.tab5_titul1 = CTkLabel(self.tabview.tab(tab5), text="COSTOS MANO DE OBRA INDIRECTA:")
            self.tab5_titul1.grid(row=0, column=0, columnspan="3", padx=20, pady=20)

            # Add a Treeview widget
            self.tree_mano_obra_indirecta = ttk.Treeview(self.tabview.tab(tab5), style="mystyle.Treeview", selectmode="extended")

            self.tree_mano_obra_indirecta['columns'] = ('id', 'cargo', 'salario_mes_historico', 'total_salario_historico', 'costo_asignado')
            self.tree_mano_obra_indirecta.column('#0', stretch=NO, width=0, anchor=CENTER)
            self.tree_mano_obra_indirecta.column('#1', stretch=NO, minwidth=100, width=50, anchor=CENTER)
            self.tree_mano_obra_indirecta.column('#2', stretch=NO, minwidth=100, width=200, anchor=CENTER)
            self.tree_mano_obra_indirecta.column('#3', stretch=NO, minwidth=100, width=200, anchor="e")
            self.tree_mano_obra_indirecta.column('#4', stretch=NO, minwidth=100, width=200, anchor="e")
            self.tree_mano_obra_indirecta.column('#5', stretch=NO, minwidth=100, width=200, anchor="e")

            self.tree_mano_obra_indirecta.heading('id', text='ID', anchor=CENTER)
            self.tree_mano_obra_indirecta.heading('cargo', text='CARGO', anchor=CENTER)
            self.tree_mano_obra_indirecta.heading('salario_mes_historico', text='SALARIO/MES', anchor=CENTER)
            self.tree_mano_obra_indirecta.heading('total_salario_historico', text='TOTAL SALARIO', anchor=CENTER)
            self.tree_mano_obra_indirecta.heading('costo_asignado', text='COSTO ASIGNADO', anchor=CENTER)
            self.tree_mano_obra_indirecta.grid(row=1, column=0,  columnspan=3, padx=20, pady=20)

            self.tab5_total1 = CTkLabel(self.tabview.tab(tab5), text="TOTALES SALARIO/MES:", fg_color="#6acee2", width=300)
            self.tab5_total1.grid(row=2, column=0, padx=20, pady=20)
            self.tab5_total2 = CTkLabel(self.tabview.tab(tab5), text="TOTALES SALARIO:", fg_color="#6acee2", width=300)
            self.tab5_total2.grid(row=2, column=1, padx=20, pady=20)
            self.tab5_total3 = CTkLabel(self.tabview.tab(tab5), text="TOTALES COSTO ASIGNADO:", fg_color="#6acee2", width=300)
            self.tab5_total3.grid(row=2, column=2, padx=20, pady=20)

            # tab6 ===========================

            self.tab6_titul1 = CTkLabel(self.tabview.tab(tab6), text="COSTOS ADMINISTRATIVOS:")
            self.tab6_titul1.grid(row=0, column=0, columnspan="2", padx=20, pady=20)

            # Add a Treeview widget
            self.tree_costos_administrativos = ttk.Treeview(self.tabview.tab(tab6), style="mystyle.Treeview", selectmode="extended")

            self.tree_costos_administrativos['columns'] = ('id', 'tipo_costo', 'costo_mes_historico', 'costo_asignado')
            self.tree_costos_administrativos.column('#0', stretch=NO, width=0, anchor=CENTER)
            self.tree_costos_administrativos.column('#1', stretch=NO, minwidth=100, width=50, anchor=CENTER)
            self.tree_costos_administrativos.column('#2', stretch=NO, minwidth=100, width=200, anchor=CENTER)
            self.tree_costos_administrativos.column('#3', stretch=NO, minwidth=100, width=300, anchor="e")
            self.tree_costos_administrativos.column('#4', stretch=NO, minwidth=100, width=300, anchor="e")

            self.tree_costos_administrativos.heading('id', text='ID', anchor=CENTER)
            self.tree_costos_administrativos.heading('tipo_costo', text='TIPO DE COSTO', anchor=CENTER)
            self.tree_costos_administrativos.heading('costo_mes_historico', text='COSTO TOTAL/MES', anchor=CENTER)
            self.tree_costos_administrativos.heading('costo_asignado', text='COSTO ASIGNADO', anchor=CENTER)
            self.tree_costos_administrativos.grid(row=1, column=0,  columnspan=2, padx=20, pady=20)

            self.tab6_total1 = CTkLabel(self.tabview.tab(tab6), text="TOTALES COSTO TOTAL/MES:", fg_color="#6acee2", width=300)
            self.tab6_total1.grid(row=2, column=0, padx=20, pady=20)
            self.tab6_total2 = CTkLabel(self.tabview.tab(tab6), text="TOTALES COSTO ASIGNADO:", fg_color="#6acee2", width=300)
            self.tab6_total2.grid(row=2, column=1, padx=20, pady=20)

            # tab7 ===========================

            self.tab7_titul1 = CTkLabel(self.tabview.tab(tab7), text="DEPRECIACIÓN MAQUINARIA:")
            self.tab7_titul1.grid(row=0, column=0, padx=20, pady=20)

            # Add a Treeview widget
            self.tree_deprecia_maquinaria = ttk.Treeview(self.tabview.tab(tab7), style="mystyle.Treeview", selectmode="extended")

            self.tree_deprecia_maquinaria['columns'] = ('id', 'tipo_bien', 'cantidad', 'valor_comercial_historico', 'valor_total', 'vida_util', 'deprecia_mes')
            self.tree_deprecia_maquinaria.column('#0', stretch=NO, width=0, anchor=CENTER)
            self.tree_deprecia_maquinaria.column('#1', stretch=NO, minwidth=100, width=50, anchor=CENTER)
            self.tree_deprecia_maquinaria.column('#2', stretch=NO, minwidth=100, width=200, anchor=CENTER)
            self.tree_deprecia_maquinaria.column('#3', stretch=NO, minwidth=100, width=150, anchor=CENTER)
            self.tree_deprecia_maquinaria.column('#4', stretch=NO, minwidth=100, width=200, anchor="e")
            self.tree_deprecia_maquinaria.column('#5', stretch=NO, minwidth=100, width=200, anchor="e")
            self.tree_deprecia_maquinaria.column('#6', stretch=NO, minwidth=100, width=150, anchor=CENTER)
            self.tree_deprecia_maquinaria.column('#7', stretch=NO, minwidth=100, width=200, anchor="e")

            self.tree_deprecia_maquinaria.heading('id', text='ID', anchor=CENTER)
            self.tree_deprecia_maquinaria.heading('tipo_bien', text='TIPO BIEN', anchor=CENTER)
            self.tree_deprecia_maquinaria.heading('cantidad', text='CANTIDAD', anchor=CENTER)
            self.tree_deprecia_maquinaria.heading('valor_comercial_historico', text='VALOR COMERCIAL', anchor=CENTER)
            self.tree_deprecia_maquinaria.heading('valor_total', text='VALOR TOTAL', anchor=CENTER)
            self.tree_deprecia_maquinaria.heading('vida_util', text='VIDA UTIL', anchor=CENTER)
            self.tree_deprecia_maquinaria.heading('deprecia_mes', text='DEPRECIACIÓN/MES', anchor=CENTER)
            self.tree_deprecia_maquinaria.grid(row=1, column=0, padx=20, pady=20)

            self.tab7_total1 = CTkLabel(self.tabview.tab(tab7), text="TOTAL DEPRECIACIÓN/MES:", fg_color="#6acee2", width=300)
            self.tab7_total1.grid(row=2, column=0, padx=20, pady=20)
            self.tab7_total2 = CTkLabel(self.tabview.tab(tab7), text="COSTO ASIGNADO:", fg_color="#6acee2", width=300)
            self.tab7_total2.grid(row=3, column=0, padx=20, pady=20)
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error:\n{e}\n{traceback.format_exc()}")
        # Fin - Hoja de Costos

        # Carga Prestacional frame
        self.carga_prestacional_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.carga_prestacional_frame.grid_columnconfigure(0, weight=1)
        self.carga_prestacional_frame_label = CTkLabel(self.carga_prestacional_frame, text="Carga Prestacional",
                                          font=CTkFont(size=15, weight="bold"))
        self.carga_prestacional_frame_label.grid(row=0, column=0, pady=10)

        # Contenedor módulo Frame
        self.cp_frame = CTkFrame(self.carga_prestacional_frame, corner_radius=10)
        self.cp_frame.grid(row=1, column=0, padx=10, pady=10, columnspan=2, sticky="nsew")

        self.cp_frame.columnconfigure(0, weight=1)
        self.cp_frame.columnconfigure(1, weight=1)
        self.cp_frame.rowconfigure(0, weight=1)
        self.cp_frame.rowconfigure(1, weight=1)

        self.frame_col1 = CTkFrame(self.cp_frame)
        self.frame_col1.grid(row=0, column=0, sticky="e", padx=10, pady=10)

        self.frame_col2 = CTkFrame(self.cp_frame)
        self.frame_col2.grid(row=0, column=1, sticky="w", padx=10, pady=10)

        self.frame_row1 = CTkFrame(self.cp_frame)
        self.frame_row1.grid(row=1, column=0, sticky="n", columnspan=2, padx=10, pady=10)

        # ============ frame_col1 ============

        self.frame_col1_label1 = CTkLabel(self.frame_col1, text="Salario mímino", fg_color="transparent")
        self.frame_col1_label1.grid(row=0, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry1 = CTkEntry(self.frame_col1, placeholder_text="salario_min", justify="right")
        self.frame_col1_entry1.grid(row=0, column=1, padx=10, pady=10)

        self.frame_col1_label3 = CTkLabel(self.frame_col1, text="Prima de Servicios", fg_color="transparent")
        self.frame_col1_label3.grid(row=1, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry3 = CTkEntry(self.frame_col1, placeholder_text="prima_servicios", justify="right")
        self.frame_col1_entry3.grid(row=1, column=1, padx=10, pady=10)

        self.frame_col1_label5 = CTkLabel(self.frame_col1, text="Intereses de Cesantías", fg_color="transparent")
        self.frame_col1_label5.grid(row=2, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry5 = CTkEntry(self.frame_col1, placeholder_text="intereses_cesantias", justify="right")
        self.frame_col1_entry5.grid(row=2, column=1, padx=10, pady=10)

        self.frame_col1_label7 = CTkLabel(self.frame_col1, text="Caja de Compensación", fg_color="transparent")
        self.frame_col1_label7.grid(row=3, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry7 = CTkEntry(self.frame_col1, placeholder_text="caja_compensacion", justify="right")
        self.frame_col1_entry7.grid(row=3, column=1, padx=10, pady=10)

        self.frame_col1_label9 = CTkLabel(self.frame_col1, text="Arl", fg_color="transparent")
        self.frame_col1_label9.grid(row=4, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry9 = CTkEntry(self.frame_col1, placeholder_text="arl", justify="right")
        self.frame_col1_entry9.grid(row=4, column=1, padx=10, pady=10)

        # ============ frame_col2 ============

        self.frame_col2_label2 = CTkLabel(self.frame_col2, text="Auxilio de Transporte", fg_color="transparent")
        self.frame_col2_label2.grid(row=0, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry2 = CTkEntry(self.frame_col2, placeholder_text="aux_transporte", justify="right")
        self.frame_col2_entry2.grid(row=0, column=1, padx=10, pady=10)

        self.frame_col2_label4 = CTkLabel(self.frame_col2, text="Auxilio de Cesantías", fg_color="transparent")
        self.frame_col2_label4.grid(row=1, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry4 = CTkEntry(self.frame_col2, placeholder_text="aux_cesantias", justify="right")
        self.frame_col2_entry4.grid(row=1, column=1, padx=10, pady=10)

        self.frame_col2_label6 = CTkLabel(self.frame_col2, text="Vacaciones", fg_color="transparent")
        self.frame_col2_label6.grid(row=2, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry6 = CTkEntry(self.frame_col2, placeholder_text="vacaciones", justify="right")
        self.frame_col2_entry6.grid(row=2, column=1, padx=10, pady=10)

        self.frame_col2_label8 = CTkLabel(self.frame_col2, text="Pensión", fg_color="transparent")
        self.frame_col2_label8.grid(row=3, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry8 = CTkEntry(self.frame_col2, placeholder_text="pension", justify="right")
        self.frame_col2_entry8.grid(row=3, column=1, padx=10, pady=10)

        self.frame_col2_label10 = CTkLabel(self.frame_col2, text="Salud", fg_color="transparent")
        self.frame_col2_label10.grid(row=4, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry10 = CTkEntry(self.frame_col2, placeholder_text="salud", justify="right")
        self.frame_col2_entry10.grid(row=4, column=1, padx=10, pady=10)

        # ============ Zona Botones ============

        self.frame_row1_label11 = CTkLabel(self.frame_row1, text="Total Costo Mensual Trabajador", fg_color="transparent")
        self.frame_row1_label11.grid(row=0, column=0, padx=10, pady=10, sticky="w")
        self.frame_row1_entry11 = CTkEntry(self.frame_row1, placeholder_text="total_costo_mensual_trabajador", justify="right")
        self.frame_row1_entry11.grid(row=0, column=1, padx=10, pady=10)

        self.b1utton = CTkButton(self.carga_prestacional_frame, text="Guardar", command=self.guardar_carga_prestacional_event)
        self.b1utton.grid(row=2, column=0, pady=10)

        # create fifth frame
        self.fifth_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.fifth_frame.grid_columnconfigure(0, weight=1)
        self.fifth_frame_label = CTkLabel(self.fifth_frame, text="Valor Minuto",
                                                       font=CTkFont(size=15, weight="bold"))
        self.fifth_frame_label.grid(row=0, column=0, padx=20, pady=10)

        # select default frame
        self.select_frame_by_name("home")

    def OnDoubleClick(self, event):
        curItem = self.tree_productos.focus()
        item = self.tree_productos.item(curItem)

        messagebox.showinfo(title=f"{item['values'][1]}, {item['values'][2]}", message=f"""ID: {item['values'][0]}
        Título: {item['values'][1]}
        Fecha: {item['values'][2]}
        Director: {item['values'][3]}
        Puntuación: {item['values'][4]}
        Notas: {item['values'][5]}""")

    def abrir_conexion(self):
        import traceback
        try:
            import os
            fullpath = os.path.join(BASE_DIR, "scp1.db")
            print(f"Ruta DB: {fullpath}")
            self.conexion = sqlite3.connect(fullpath)
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error:\n{e}\n{traceback.format_exc()}\n{fullpath}")
        return self.conexion

    def open_input_dialog_event(self):
        dialog = CTkInputDialog(text="Type in a number:", title="CTkInputDialog")
        print("CTkInputDialog:", dialog.get_input())

    def configuraciones_iniciales(self):
        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_config where param='tema'"
            cursor.execute(sql)
            registro = cursor.fetchone()
            # print(registro["value1"])
            self.change_appearance_mode_event(registro["value1"])
            # messagebox.showinfo(title="SCP", message="Conexión establecida!")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
        finally:
            self.conexion.close()

    def select_frame_by_name(self, name):
        # set button color for selected button
        self.home_button.configure(fg_color=("gray75", "gray25") if name == "home" else "transparent")
        self.productos_menu.configure(fg_color=("gray75", "gray25") if name == "productos" else "transparent")
        self.materiales_menu.configure(fg_color=("gray75", "gray25") if name == "materiales" else "transparent")
        self.frame_3_button.configure(fg_color=("gray75", "gray25") if name == "frame_3" else "transparent")
        self.carga_prestacional_menu.configure(fg_color=("gray75", "gray25") if name == "carga" else "transparent")
        self.frame_5_button.configure(fg_color=("gray75", "gray25") if name == "frame_5" else "transparent")

        # show selected frame
        if name == "home":
            self.home_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.home_frame.grid_forget()

        if name == "productos":
            self.productos_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.productos_frame.grid_forget()

        if name == "materiales":
            self.materiales_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.materiales_frame.grid_forget()

        if name == "frame_3":
            self.third_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.third_frame.grid_forget()

        if name == "carga":
            self.carga_prestacional_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.carga_prestacional_frame.grid_forget()

        if name == "frame_5":
            self.fifth_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.fifth_frame.grid_forget()

    def home_button_event(self):
        self.select_frame_by_name("home")

    def productos_menu_event(self):
        self.select_frame_by_name("productos")

    def materiales_menu_event(self):
        self.select_frame_by_name("materiales")

    def frame_3_button_event(self):
        self.select_frame_by_name("frame_3")

    def carga_prestacional_event(self):
        # Habilitar los campos de sólo lectura para cálculos y luego volver a deshabilitarlos en cada caso
        self.frame_col1_entry1.configure(fg_color=('white', 'white'), text_color=('black', 'black'))
        self.frame_col2_entry2.configure(fg_color=('white', 'white'), text_color=('black', 'black'))

        self.frame_col1_entry3.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col1_entry5.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col1_entry7.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col1_entry9.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry4.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry6.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry8.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry10.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))

        self.frame_row1_entry11.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))

        try:
            import locale
            locale.setlocale(locale.LC_ALL, 'es_CO.utf8')  # COLOMBIA

            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_config"
            cursor.execute(sql)
            datos = cursor.fetchall()

            for i in datos:
                if i["param"] == "salario_min":
                    self.salario_min = float(i["value1"])
                    self.frame_col1_entry1.delete(0, END)
                    self.frame_col1_entry1.insert(0, locale.format_string("%.2f", self.salario_min, grouping=True))
                elif i["param"] == "aux_transporte":
                    self.aux_transporte = float(i["value1"])
                    self.frame_col2_entry2.delete(0, END)
                    self.frame_col2_entry2.insert(0, locale.format_string("%.2f", self.aux_transporte, grouping=True))
                elif i["param"] == "prima_servicios":
                    self.prima_servicios = self.salario_min * (float(i["value2"])/100)
                    self.frame_col1_entry3.delete(0, END)
                    self.frame_col1_entry3.insert(0, locale.format_string("%.2f", self.prima_servicios, grouping=True))
                    self.frame_col1_entry3.configure(state='readonly')
                elif i["param"] == "aux_cesantias":
                    self.aux_cesantias = self.salario_min * (float(i["value2"])/100)
                    self.frame_col2_entry4.delete(0, END)
                    self.frame_col2_entry4.insert(0, locale.format_string("%.2f", self.aux_cesantias, grouping=True))
                    self.frame_col2_entry4.configure(state='readonly')
                elif i["param"] == "intereses_cesantias":
                    self.intereses_cesantias = self.salario_min * (float(i["value2"])/100)
                    self.frame_col1_entry5.delete(0, END)
                    self.frame_col1_entry5.insert(0, locale.format_string("%.2f", self.intereses_cesantias, grouping=True))
                    self.frame_col1_entry5.configure(state='readonly')
                elif i["param"] == "vacaciones":
                    self.vacaciones = self.salario_min * (float(i["value2"])/100)
                    self.frame_col2_entry6.delete(0, END)
                    self.frame_col2_entry6.insert(0, locale.format_string("%.2f", self.vacaciones, grouping=True))
                    self.frame_col2_entry6.configure(state='readonly')
                elif i["param"] == "caja_compensacion":
                    self.caja_compensacion = self.salario_min * (float(i["value2"])/100)
                    self.frame_col1_entry7.delete(0, END)
                    self.frame_col1_entry7.insert(0, locale.format_string("%.2f", self.caja_compensacion, grouping=True))
                    self.frame_col1_entry7.configure(state='readonly')
                elif i["param"] == "pension":
                    self.pension = self.salario_min * (float(i["value2"])/100)
                    self.frame_col2_entry8.delete(0, END)
                    self.frame_col2_entry8.insert(0, locale.format_string("%.2f", self.pension, grouping=True))
                    self.frame_col2_entry8.configure(state='readonly')
                elif i["param"] == "arl":
                    self.arl = self.salario_min * (float(i["value2"])/100)
                    self.frame_col1_entry9.delete(0, END)
                    self.frame_col1_entry9.insert(0, locale.format_string("%.2f", self.arl, grouping=True))
                    self.frame_col1_entry9.configure(state='readonly')
                elif i["param"] == "salud":
                    self.salud = self.salario_min * (float(i["value2"])/100)
                    self.frame_col2_entry10.delete(0, END)
                    self.frame_col2_entry10.insert(0, locale.format_string("%.2f", self.salud, grouping=True))
                    self.frame_col2_entry10.configure(state='readonly')
                elif i["param"] == "total_costo_mensual_trabajador":
                    self.total_costo_mensual_trabajador = (self.salario_min * (float(i["value2"])/100)) - self.salud + self.aux_transporte + self.salario_min
                    self.frame_row1_entry11.delete(0, END)
                    self.frame_row1_entry11.insert(0, locale.format_string("%.2f", self.total_costo_mensual_trabajador, grouping=True))
                    self.frame_row1_entry11.configure(state='readonly')

        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error recuperando los datos.\n{e}")
        finally:
            self.conexion.close()

        self.select_frame_by_name("carga")

    def carga_prestacional_event_vieja(self):
        try:
            import locale
            locale.setlocale(locale.LC_ALL, 'es_CO.utf8')  # COLOMBIA

            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_config"
            cursor.execute(sql)
            datos = cursor.fetchall()

            for i in datos:
                if i["param"] == "salario_min":
                    self.frame_col1_entry1.delete(0, END)
                    self.frame_col1_entry1.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "aux_transporte":
                    self.frame_col2_entry2.delete(0, END)
                    self.frame_col2_entry2.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "prima_servicios":
                    self.frame_col1_entry3.delete(0, END)
                    self.frame_col1_entry3.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "aux_cesantias":
                    self.frame_col2_entry4.delete(0, END)
                    self.frame_col2_entry4.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "intereses_cesantias":
                    self.frame_col1_entry5.delete(0, END)
                    self.frame_col1_entry5.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "vacaciones":
                    self.frame_col2_entry6.delete(0, END)
                    self.frame_col2_entry6.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "caja_compensacion":
                    self.frame_col1_entry7.delete(0, END)
                    self.frame_col1_entry7.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "pension":
                    self.frame_col2_entry8.delete(0, END)
                    self.frame_col2_entry8.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "arl":
                    self.frame_col1_entry9.delete(0, END)
                    self.frame_col1_entry9.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "salud":
                    self.frame_col2_entry10.delete(0, END)
                    self.frame_col2_entry10.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))
                elif i["param"] == "total_costo_mensual_trabajador":
                    self.frame_row1_entry11.delete(0, END)
                    self.frame_row1_entry11.insert(0, locale.format_string("%.2f", float(i["value1"]), grouping=True))

        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error recuperando los datos.\n{e}")
        finally:
            self.conexion.close()

        self.select_frame_by_name("carga")

    def guardar_carga_prestacional_event(self):
        records_to_update = [
            (self.frame_col1_entry1.get().replace(".", "").replace(",", "."), "salario_min"),
            (self.frame_col2_entry2.get().replace(".", "").replace(",", "."), "aux_transporte")
        ]

        try:
            self.abrir_conexion()
            cursor = self.conexion.cursor()
            sql = "update scp_config set value1 = ? where param = ?"
            cursor.executemany(sql, records_to_update)
            self.conexion.commit()
            self.conexion.close()
            # Actualizar resto de datos después de guardar el salario mínimo y auxilio de transporte
            self.carga_prestacional_event()
            records_to_update = [
                (self.frame_col1_entry3.get().replace(".", "").replace(",", "."), "prima_servicios"),
                (self.frame_col2_entry4.get().replace(".", "").replace(",", "."), "aux_cesantias"),
                (self.frame_col1_entry5.get().replace(".", "").replace(",", "."), "intereses_cesantias"),
                (self.frame_col2_entry6.get().replace(".", "").replace(",", "."), "vacaciones"),
                (self.frame_col1_entry7.get().replace(".", "").replace(",", "."), "caja_compensacion"),
                (self.frame_col2_entry8.get().replace(".", "").replace(",", "."), "pension"),
                (self.frame_col1_entry9.get().replace(".", "").replace(",", "."), "arl"),
                (self.frame_col2_entry10.get().replace(".", "").replace(",", "."), "salud"),
                (self.frame_row1_entry11.get().replace(".", "").replace(",", "."), "total_costo_mensual_trabajador")
            ]
            self.abrir_conexion()
            cursor = self.conexion.cursor()
            sql = "update scp_config set value1 = ? where param = ?"
            cursor.executemany(sql, records_to_update)
            self.conexion.commit()
            messagebox.showinfo(title="SCP", message="Carga prestacional guardada!")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error guardando en base de datos.\n{e}")
        finally:
            self.conexion.close()

    def frame_5_button_event(self):
        self.select_frame_by_name("frame_5")

    def optionmenu_callback(self, choice):
        print("Producto:", choice)
        self.hoja_costos_producto = StringVar(value=choice)
        self.tab1_CIF_total_acumulado = 0
        self.tab2_titul1.configure(text=f"Producto: {self.hoja_costos_producto.get()}")

        id = choice.split(".")[0]
        # consulta resumen, tab1 ========================================
        print("tab1")
        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_producto sp inner join scp_hoja_costo hc on sp.id = hc.producto and hc.producto = ? "
            cursor.execute(sql, id)
            registro = cursor.fetchone()

            if registro is None or len(registro) == 0:
                raise sqlite3.Warning("La consulta no trajo datos.")

            self.tab1_ref_valor.configure(text=registro["referencia"])
            self.tab1_tiempoE_valor.configure(text=registro["tiempo_estandar"])
            self.tab1_fecha_valor.configure(text=registro["fecha_creacion"])
            self.tab1_eficiencia_valor.configure(text=f"{registro['eficiencia']}%")
            self.tab1_jornadaL_valor.configure(text=registro["jornada"])
            self.tab1_num_operarios_valor.configure(text=registro["operarios"])
            self.tab1_min_mes_valor.configure(text=registro["minutos_mes"])
            self.tab1_utilidad = registro['utilidad'] / 100
            self.tab1_utilidad_valor.configure(text=f"{registro['utilidad']}%")
            self.tab1_num_operarios_valor.configure(text=registro["operarios"])

            self.tab1_u_prod_mes_valor_calculo = (registro["minutos_mes"] / registro["tiempo_estandar"] * registro["operarios"]) * (registro["eficiencia"]/100)

            # actualizo dato en tab1
            self.tab1_u_prod_mes_valor.configure(text=(f"${self.tab1_u_prod_mes_valor_calculo:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
        except sqlite3.Warning as e:
            self.tab1_ref_valor.configure(text="")
            self.tab1_tiempoE_valor.configure(text="")
            self.tab1_fecha_valor.configure(text="")
            self.tab1_eficiencia_valor.configure(text="")
            self.tab1_jornadaL_valor.configure(text="")
            self.tab1_num_operarios_valor.configure(text="")
            self.tab1_min_mes_valor.configure(text="")
            self.tab1_utilidad_valor.configure(text="")
            self.tab1_num_operarios_valor.configure(text="")

            self.tab1_mat_directo_valor.configure(text="---")
            self.tab1_u_prod_mes_valor.configure(text="---")
            self.tab1_MOD_valor.configure(text="---")
            self.tab1_CIF_valor.configure(text="---")
            self.tab1_COSTO_TOTAL_valor.configure(text="---")
            self.tab1_precio_venta_valor.configure(text="---")

            # self.tab1_u_prod_mes_valor_calculo = 0

            messagebox.showinfo(title="SCP", message=f"{e} en la hoja Resumen")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error:\n{e}")
        finally:
            self.conexion.close()

        # consulta costo de materiales directos, tab2 ==================================
        print("tab2")
        try:
            # Clear the data in Treeview widget
            self.tree_materiales_directos.delete(*self.tree_materiales_directos.get_children())

            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = """SELECT * from scp_hoja_material shm 
            inner join scp_hoja_costo shc on shm.hoja_costo_id = shc.id 
            inner join scp_material sm on shm.material_id = sm.id 
            inner join scp_producto sp on shc.producto = sp.id 
            where shm.hoja_costo_id = ?"""
            cursor.execute(sql, id)
            registro = cursor.fetchall()

            if registro is None or len(registro) == 0:
                raise sqlite3.Warning("La consulta no trajo datos.")

            # Insert the data in Treeview widget
            self.total_costo_materiales_directos = 0
            for r in registro:
                self.total_costo_materiales_directos += (r["consumo_unit"] * r['costo_unit_historico'])
                self.tree_materiales_directos.insert('', 'end', values=(r["id"], r["nombre_tipo"], r["proveedor"], r["unidad_med"], r["consumo_unit"], (f"${r['costo_unit_historico']:,.1f}").replace(',','*').replace('.', ',').replace('*','.'), (f"${(r['consumo_unit'] * r['costo_unit_historico']):,.0f}").replace(',','*').replace('.', ',').replace('*','.') ))
                # self.tree_materiales_directos.insert("", END, values=r)

            self.tab2_total.configure(text=(f"TOTALES: ${self.total_costo_materiales_directos:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            # actualizo dato en tab1
            self.tab1_mat_directo_valor.configure(text=(f"${self.total_costo_materiales_directos:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
        except sqlite3.Warning as e:
            messagebox.showinfo(title="SCP", message=f"{e} en materiales directos")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error:\n{e}")
        finally:
            self.conexion.close()

        # consulta costo de mano de obra directa, tab3 ==================================
        print("tab3")
        import traceback
        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_config"
            cursor.execute(sql)
            datos = cursor.fetchall()

            if datos is None or len(datos) == 0:
                raise sqlite3.Warning("La consulta no trajo datos.")

            for i in datos:
                if i["param"] == "salario_min":
                    self.salario_min = float(i["value1"])
                    self.tab3_sal_basico_valor.configure(text=(f"${self.salario_min:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                if i["param"] == "salud":
                    self.salud = self.salario_min * (float(i["value2"]) / 100)

                if i["param"] == "aux_transporte":
                    self.aux_transporte = float(i["value1"])
                    self.tab3_sub_transp_valor.configure(text=(f"${self.aux_transporte:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                if i["param"] == "total_costo_mensual_trabajador":
                    self.porcentaje_carga_prestacional = (float(i["value2"]) / 100)
                    self.tab3_carga_pres_valor.configure(text=(f"{self.porcentaje_carga_prestacional*100:,.2f}%").replace(',', '*').replace('.', ',').replace('*', '.'))

                    self.total_costo_mensual_trabajador = (self.salario_min * (float(i["value2"]) / 100)) - self.salud + self.aux_transporte + self.salario_min
                    self.tab3_total_sal_valor.configure(text=(f"${self.total_costo_mensual_trabajador:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
                    self.tab3_total_sal_otros_valor.configure(text=(f"${self.total_costo_mensual_trabajador:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

            # Cálculos especiales tab3
            try:
                self.tab3_mod = int(self.tab1_num_operarios_valor.cget('text')) * self.total_costo_mensual_trabajador
                self.tab3_costo_mod_valor.configure(text=(f"${self.tab3_mod:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            except:
                self.tab3_mod = 0
                self.tab3_costo_mod_valor.configure(text="---")

            try:
                self.tab3_min_efectivo = int(self.tab1_num_operarios_valor.cget('text')) * 24 * 8 * 60
            except:
                self.tab3_min_efectivo = 0
            self.tab3_min_mes_valor.configure(text=(f"${self.tab3_min_efectivo:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

            try:
                self.tab3_factor_minuto = self.tab3_mod / self.tab3_min_efectivo
            except:
                self.tab3_factor_minuto = 0
            self.tab3_fact_min_valor.configure(text=(f"${self.tab3_factor_minuto:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

            try:
                self.tab3_costo_unit_mod = self.tab3_factor_minuto * float(self.tab1_tiempoE_valor.cget("text"))
                # actualizo dato en tab1
                self.tab1_MOD_valor.configure(text=(f"${self.tab3_costo_unit_mod:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            except:
                self.tab3_costo_unit_mod = 0
            self.tab3_unitario_mod_valor.configure(text=(f"${self.tab3_costo_unit_mod:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

        except sqlite3.Warning as e:
            messagebox.showinfo(title="SCP", message=f"{e} en mano de obra directa")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error:\n{e}\n{traceback.format_exc()}")
        finally:
            self.conexion.close()

        # consulta costo de materiales indirectos, tab4 ==================================
        print("tab4")
        try:
            # Clear the data in Treeview widget
            self.tree_materiales_indirectos.delete(*self.tree_materiales_indirectos.get_children())

            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = """SELECT * from scp_hoja_material_indirecto shmi
            inner join scp_hoja_costo shc on shmi.hoja_costo_id = shc.id
            inner join scp_material sm on shmi.material_id = sm.id 
            inner join scp_producto sp on shc.producto = sp.id 
            where shmi.hoja_costo_id = ?"""
            cursor.execute(sql, id)
            registro = cursor.fetchall()

            if registro is None or len(registro) == 0:
                raise sqlite3.Warning("La consulta no trajo datos.")

            # Insert the data in Treeview widget
            self.total_costo_materiales_indirectos = 0
            self.total_costo_materiales_indirectos_asignado = 0
            for r in registro:
                self.total_costo_materiales_indirectos += (r["consumo_mes"] * r['costo_historico'])
                self.total_costo_materiales_indirectos_asignado += (r["consumo_mes"] * r['costo_historico']) / self.tab1_u_prod_mes_valor_calculo

                self.tree_materiales_indirectos.insert('', 'end', values=(
                    r["id"], r["nombre_tipo"], r["unidad_med"], r["consumo_mes"],
                    (f"${r['costo_historico']:,.1f}").replace(',', '*').replace('.', ',').replace('*', '.'),
                    (f"${(r['consumo_mes'] * r['costo_historico']):,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'),
                    (f"${(r['consumo_mes'] * r['costo_historico']) / self.tab1_u_prod_mes_valor_calculo:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.')))
                # self.tree_materiales_indirectos.insert("", END, values=r)

            self.tab4_total1.configure(text=(f"TOTALES: ${self.total_costo_materiales_indirectos:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            self.tab4_total2.configure(text=(f"TOTALES ASIGNADO: ${self.total_costo_materiales_indirectos_asignado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

            # actualizo dato en tab1
            self.tab1_CIF_total_acumulado += self.total_costo_materiales_indirectos_asignado
            self.tab1_CIF_valor.configure(text=(f"${self.tab1_CIF_total_acumulado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
        except sqlite3.Warning as e:
            messagebox.showinfo(title="SCP", message=f"{e} en materiales indirectos")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error:\n{e}")
        finally:
            self.conexion.close()

        # consulta costo de materiales indirectos, tab5 ==================================
        print("tab5")
        try:
            # Clear the data in Treeview widget
            self.tree_mano_obra_indirecta.delete(*self.tree_mano_obra_indirecta.get_children())

            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = """SELECT * from scp_hoja_mano_obra_indirecta shmoi 
            inner join scp_cargos_mano_obra scmo on shmoi.cargos_id = scmo.id  
            where shmoi.hoja_costo_id = ?"""
            cursor.execute(sql, id)
            registro = cursor.fetchall()

            if registro is None or len(registro) == 0:
                raise sqlite3.Warning("La consulta no trajo datos.")

            # Insert the data in Treeview widget
            self.total_salario_mes = 0
            self.total_salario = 0
            self.total_costo_mano_obra_indirecta_asignado = 0
            for r in registro:
                if r["cargo"] == "GERENTE":
                    salario = r["salario_mes_historico"]
                else:
                    salario = r["salario_mes_historico"] + (r["salario_mes_historico"] * self.porcentaje_carga_prestacional)

                self.total_salario_mes += r["salario_mes_historico"]
                self.total_salario += salario

                costo_asignado = salario / self.tab1_u_prod_mes_valor_calculo
                self.total_costo_mano_obra_indirecta_asignado += costo_asignado

                self.tree_mano_obra_indirecta.insert('', 'end', values=(
                    r["id"], r["cargo"], (f"${r['salario_mes_historico']:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'),
                    (f"${salario:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'),
                    (f"${costo_asignado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.')))
                # self.tree_mano_obra_indirecta.insert("", END, values=r)

            self.tab5_total1.configure(text=(f"TOTALES SALARIO/MES: ${self.total_salario_mes:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            self.tab5_total2.configure(text=(f"TOTALES SALARIO: ${self.total_salario:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            self.tab5_total3.configure(text=(f"TOTALES COSTO ASIGNADO: ${self.total_costo_mano_obra_indirecta_asignado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

            # actualizo dato en tab1
            self.tab1_CIF_total_acumulado += self.total_costo_mano_obra_indirecta_asignado
            self.tab1_CIF_valor.configure(text=(f"${self.tab1_CIF_total_acumulado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
        except sqlite3.Warning as e:
            messagebox.showinfo(title="SCP", message=f"{e} en mano de obra indirecta")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error:\n{e}")
        finally:
            self.conexion.close()

        # consulta costo de materiales indirectos, tab6 ==================================
        print("tab6")
        try:
            # Clear the data in Treeview widget
            self.tree_costos_administrativos.delete(*self.tree_costos_administrativos.get_children())

            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = """SELECT * from scp_hoja_costos_administrativos shca  
            inner join scp_costos_admon sca on shca.costos_admin_id = sca.id  
            where shca.hoja_costo_id = ?"""
            cursor.execute(sql, id)
            registro = cursor.fetchall()

            if registro is None or len(registro) == 0:
                raise sqlite3.Warning("La consulta no trajo datos.")

            # Insert the data in Treeview widget
            self.total_costo_total_mes = 0
            self.total_costos_administrativos_asignado = 0

            for r in registro:
                self.total_costo_total_mes += r["costo_mes_historico"]

                total_costos_administrativos_asignado = r["costo_mes_historico"] / self.tab1_u_prod_mes_valor_calculo
                self.total_costos_administrativos_asignado += total_costos_administrativos_asignado

                self.tree_costos_administrativos.insert('', 'end', values=(
                    r["id"], r["tipo_costo"], (f"${r['costo_mes_historico']:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.'),
                    (f"${total_costos_administrativos_asignado:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.')))
                # self.tree_costos_administrativos.insert("", END, values=r)

            self.tab6_total1.configure(text=(f"TOTALES COSTO TOTAL/MES: ${self.total_costo_total_mes:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            self.tab6_total2.configure(text=(f"TOTALES COSTO ASIGNADO: ${self.total_costos_administrativos_asignado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

            # actualizo dato en tab1
            self.tab1_CIF_total_acumulado += self.total_costos_administrativos_asignado
            self.tab1_CIF_valor.configure(text=(f"${self.tab1_CIF_total_acumulado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
        except sqlite3.Warning as e:
            messagebox.showinfo(title="SCP", message=f"{e} en costos administrativos")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error:\n{e}")
        finally:
            self.conexion.close()

        # consulta costo de materiales indirectos, tab7 ==================================
        print("tab7")
        try:
            # Clear the data in Treeview widget
            self.tree_deprecia_maquinaria.delete(*self.tree_deprecia_maquinaria.get_children())

            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = """SELECT * from scp_hoja_depreciacion_maquinaria shdm  
            inner join scp_maquinaria sm on shdm.bien_id  = sm.id  
            where shdm.hoja_costo_id = ?"""
            cursor.execute(sql, id)
            registro = cursor.fetchall()

            if registro is None or len(registro) == 0:
                raise sqlite3.Warning("La consulta no trajo datos.")

            # Insert the data in Treeview widget
            self.total_depreciacion_mes = 0
            self.total_depreciacion_maquinaria_mes = 0

            for r in registro:
                self.total_depreciacion_mes += r["valor_comercial_historico"]

                total_depreciacion_maquinaria_mes = r["cantidad"] * r["valor_comercial_historico"]
                self.total_depreciacion_maquinaria_mes += total_depreciacion_maquinaria_mes / r["vida_util"]

                self.tree_deprecia_maquinaria.insert('', 'end', values=(
                    r["id"], r["tipo_bien"], r["cantidad"], (f"${r['valor_comercial_historico']:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.'),
                    (f"${total_depreciacion_maquinaria_mes:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'), r["vida_util"],
                    (f"${total_depreciacion_maquinaria_mes / r['vida_util']:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.')))
                # self.tree_deprecia_maquinaria.insert("", END, values=r)

            self.tab7_total1.configure(text=(f"TOTAL DEPRECIACION /MES: ${self.total_depreciacion_maquinaria_mes:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            costo_asignado_total = self.total_depreciacion_maquinaria_mes / self.tab1_u_prod_mes_valor_calculo
            self.tab7_total2.configure(text=(f"COSTO ASIGNADO: ${costo_asignado_total:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

            # actualizo dato en tab1
            self.tab1_CIF_total_acumulado += costo_asignado_total
            self.tab1_CIF_valor.configure(text=(f"${self.tab1_CIF_total_acumulado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

            tab1_COSTO_TOTAL = self.total_costo_materiales_directos + self.tab3_costo_unit_mod + self.tab1_CIF_total_acumulado
            self.tab1_COSTO_TOTAL_valor.configure(text=(f"${tab1_COSTO_TOTAL:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

            tab1_PRECIO_VENTA = tab1_COSTO_TOTAL / (1 - self.tab1_utilidad)
            self.tab1_precio_venta_valor.configure(text=(f"${tab1_PRECIO_VENTA:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
        except sqlite3.Warning as e:
            messagebox.showinfo(title="SCP", message=f"{e} en depreciación de maquinaria")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error:\n{e}")
        finally:
            self.conexion.close()


    # fin callback - hoja de costos

    def change_appearance_mode_event(self, modo=False):
        if modo:
            # Modificamos la apariencia desde la base de datos
            set_appearance_mode(modo)
            # Cambiamos valor a varible <class 'tkinter.StringVar'>
            self.selected_option.set(modo)
        else:
            # Modificamos la apariencia desde el menú de configuración
            set_appearance_mode(self.selected_option.get())
            # Guardamos cambios en DB
            try:
                self.abrir_conexion()
                cursor = self.conexion.cursor()
                sql = "update scp_config set value1 = ? where param = 'tema'"
                datos = (self.selected_option.get(),)
                cursor.execute(sql, datos)
                self.conexion.commit()
                # messagebox.showinfo(title="SCP", message="Configuración guardada!")
            except Exception as e:
                messagebox.showerror(title="SCP", message=f"Error guardando en base de datos.\n{e}")
            finally:
                self.conexion.close()

    def help_menu_event(self):
        # Crear una ventana secundaria.
        ventana_secundaria = CTkToplevel()
        ventana_secundaria.resizable(False, False)

        # icon
        ventana_secundaria.wm_iconbitmap()
        ventana_secundaria.after(300, lambda: ventana_secundaria.iconphoto(False, self.iconpath))
        # fin - icon

        # quitar minimizar....
        ventana_secundaria.title("Ventana secundaria")
        ventana_secundaria.config(width=300, height=200)
        # Crear un botón dentro de la ventana secundaria
        # para cerrar la misma.
        boton_cerrar = CTkButton(
            ventana_secundaria,
            text="Cerrar ventana",
            command=ventana_secundaria.destroy
        )
        boton_cerrar.place(x=75, y=75)
        ventana_secundaria.focus()
        # Modal...
        ventana_secundaria.transient(self)  # dialog window is related to main
        ventana_secundaria.wait_visibility()
        ventana_secundaria.grab_set()
        ventana_secundaria.wait_window()

    def help_menu_about_event(self):
        messagebox.showinfo("Acerca de ...", "\nProducto Técnico-Pedagógico:\n\nSIMULADOR DE COSTOS DE PRODUCCIÓN "
                                             "PARA LA CONFECCIÓN DE PRENDAS DE VESTIR\n\n\nSENA - Centro de Formación "
                                             "en Diseño, Confección y Moda\n\nColombia - 2023")


if __name__ == "__main__":
    set_appearance_mode("system")  # light | dark | system
    set_default_color_theme(f"{BASE_DIR}/purple.json")
    # set_default_color_theme("green")

    app = App()

    # app.iconbitmap(BASE_DIR/"favicon_io/favicon.ico")
    # app.wm_iconbitmap(BASE_DIR/"favicon_io/favicon_windows.ico")

    # app.after(201, lambda: app.iconbitmap(BASE_DIR/"favicon_io/favicon_windows.ico"))

    # icono_chico = PhotoImage(file=BASE_DIR/"favicon_io/favicon-16x16.png")
    # icono_grande = PhotoImage(file=BASE_DIR/"favicon_io/favicon-32x32.png")
    # app.iconphoto(False, icono_grande, icono_chico)

    import ctypes

    myappid = 'net.tikole.scp.1'  # arbitrary string
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

    app.title("Simulador de Costos de Producción")
    app.resizable(True, True)

    app.state('normal')

    app.configuraciones_iniciales()

    app.mainloop()
