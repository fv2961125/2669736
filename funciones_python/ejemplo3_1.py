# Parámetro por defecto

def preparar_cafe(cantidad = 3):
	print(f"Disfrute su rico cafe con {cantidad} de azúcar.")

if __name__ == "__main__":
	preparar_cafe(2)
	preparar_cafe(1)
	preparar_cafe()