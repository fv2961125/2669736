# Scope (Alcance) variables. Funciones

def prueba(num1):
	global centro		# tipos de datos primitivos.
	centro = "mobiliario"
	print(num1)

if __name__ == "__main__":
	centro = "moda"
	print(f"main: {centro}")
	prueba(25)
	print(f"después: {centro}")

