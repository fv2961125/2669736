"""****Ejercicio 1
Construir un programa usando funciones que permita calcular cuántos primos hay desde el 0 al N pedido al usuario.
Guardar los primos encontrados en una lista.
Imprimir todo.
"""


def primo_o_no(num):
	es_primo = True
	if num % 2 == 0 and num != 2:
		es_primo = False
	else:
		cont = 1
		for i in range(1, num, 2):
			if num % i == 0:
				cont += 1

			if cont == 3:
				es_primo = False
				break
	
	return es_primo


if __name__ == "__main__":
	lista_primos = []
	dato = int(input("Digite un num entero: "))

	for x in range(dato+1):
		if primo_o_no(x):
			# print(f"Es primo {x}")
			lista_primos.append(x)
		else:
			# print(f"NO Es primo {x}")
			pass

	print(f"Los primos encontrados entre 0 y {dato} son: {lista_primos} para un total de {len(lista_primos)}")


