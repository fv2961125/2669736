def pintar(*args):
	for i in args:
		print(f"Valor {i} - Tipo: {type(i)}")
		if type(i) == list:
			print("Si es una lista")
			for z in i:
				print(z)
		elif type(i) == dict:
			print("Si es un diccionario")
			for clave, valor in i.items():
				print(f"{clave}: {valor}")

def capturar_personas():
	global poder
	poder = "sena"
	print(poder)


if __name__ == "__main__":

	poder = "notarial"
	capturar_personas()
	print(f"main: {poder}")

	pintar([1,2,3], True, 8, 20.5, "Hola", {"nombre": "ana", "edad": 26})
