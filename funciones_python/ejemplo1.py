def promedio(e1, e2, e3):
    return (e1+e2+e3)/3


if __name__ == "__main__":
    e1 = int(input("Digite la edad 1: "))
    e2 = int(input("Digite la edad 2: "))
    e3 = int(input("Digite la edad 3: "))

    r = promedio(e1, e2, e3)

    if r > 50:
        print("Adultos")
    else:
        print("Jóvenes")
