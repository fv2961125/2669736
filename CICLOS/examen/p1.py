valor_boleta = 30000
personas = int(input("Cantidad de personas en partido?: "))

cont = 1
recaudo = 0
descontado = 0
while cont < personas:
    estrato = int(input("Cuál es su estrato? (1-6): "))
    edad = int(input("Edad: "))
    if estrato == 1 and edad < 18:
        tot = valor_boleta * 0.8
        porcentaje = valor_boleta * 0.2
    elif estrato == 1 and edad >= 18:
        tot = valor_boleta * 0.85
        porcentaje = valor_boleta * 0.15
    elif estrato == 2 and edad < 18:
        tot = valor_boleta * 0.9
        porcentaje = valor_boleta * 0.10
    elif estrato == 2 and edad >= 18:
        tot = valor_boleta * 0.95
        porcentaje = valor_boleta * 0.05
    else:
        tot = valor_boleta
        porcentaje = 0

    recaudo += tot
    descontado += porcentaje

    cont += 1
print(f"El valor total recaudado es: ${recaudo}")
print(f"El valor total en descuentos es: ${descontado}")
