# Ciclo for.
# Bucle, loop, iteraciones, vueltas, ciclos, repeticiones
"""
#           i              string, vector o lista, archivo
for __variable_iteradora__  in elemento_a_iterar:
    pass
"""

# Función generadora de números, para las iteraciones o repeticiones
"""
for num in range(20):      # [0,1,2,..., 18,19]
    if num == 7 or num == 15:
        pass
    else:
        print(num)
"""

# Calcular el promedio de 4 edades. Usar for.
"""
suma = 0
for i in range(1, 5):
    edad = int(input(f"Digite la edad {i}: "))
    suma += edad
print(f"El promedio es {suma/4}")
"""
"""
# Mostrar los números impares entre 1 y 50
# Con trampa, con for.
for i in range(1, 51, 2):
    print(i)
"""

# mostrar los números entre un N1 y un N2, dado por el usuario

n1 = int(input("Digite un número pequeño. Incluso negativo: "))
n2 = int(input("Digite un número mayor que el anterior. Incluso negativo: "))

for i in range(n1, n2+1):
    print(i)






