palabra = "hola mundo"
print(len(palabra))
for i in palabra:               # [h,o,l,a, ,m,u,n,d,o]
    print(i)

for i in range(len(palabra)):   # [0,1,2,3,4,5,6,7,8,9]
    print(palabra[i])

listado = ["hola", "mundo"]
print(len(listado))
for i in listado:               # ["hola", "mundo"]
    print(i)

for i in range(len(listado)):   # [0,1]
    print(listado[i])

# jor@misena.edu.co
