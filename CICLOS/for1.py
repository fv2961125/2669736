# For - Lectura de String. Cadena de caracteres.

frase = "Los Aprendices de ADSO del Sena"
tam = len(frase)

print(f"El tamaño es {tam}")
# frase = input("Digite una frase: ")

# Averiguar cuantas vocales hay de cada una.
cont_a = 0
cont_e = 0
cont_i = 0
cont_o = 0
cont_u = 0
cont_letras = 0
for i in frase.lower():
    if i == "a":
        cont_a += 1
    elif i == "e":
        cont_e += 1
    elif i == "i":
        cont_i += 1
    elif i == "o":
        cont_o += 1
    elif i == "u":
        cont_u += 1

    cont_letras += 1
    # print(i)

print(f"""
La cantidad de letras de la frase es {cont_letras}.
El tamaño de la frase es {cont_letras}

Hay {cont_a} aes en la frase.
Hay {cont_e} ees en la frase.
Hay {cont_i} ies en la frase.
Hay {cont_o} oes en la frase.
Hay {cont_u} ues en la frase.
""")

# Ejercicio2. Pedir la frase al usuario.
# Ejercicio3. Decir de qué tamaño es la frase.
