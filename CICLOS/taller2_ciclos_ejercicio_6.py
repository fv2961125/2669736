# Forma1: usar "slicing" de cadenas o listas -> Tomar una porción de ella
"""
abc = "zyxwvutsrqpoñnmlkjihgfedcba"
for letra in range(len(abc)):
    # Quitar la primera letra y modificar la cadena
    abc = abc[1:]
    # Quitar la última letra y modificar la cadena
    # abc = abc[:-1]
    print(abc)
"""
"""
# Forma2: Común y corriente con ciclo aninado
abc = "zyxwvutsrqpoñnmlkjihgfedcba"
inicio = 0

for i in range(len(abc)):  # 27
    cadena = ""
    for z in range(inicio, len(abc)):
        # print(abc[z], end="")
        cadena += abc[z]

    print(cadena)
    inicio += 1
"""
# Forma 3. Con listas o vectores
abc = "zyxwvutsrqpoñnmlkjihgfedcba"
datos = []
for i in abc:
    datos.append(i)

for i in range(len(datos)):
    print("")
    for z in datos:
        print(z, end="")
    datos[i] = ""

"""
for i in range(len(datos)):
    print("")
    for z in datos:
        print(z, end="")
    datos.pop(0)
"""
