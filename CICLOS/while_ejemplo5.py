# Pedir números hasta que se digite un cero. Sumarlos.
# Usar While True

suma = 0
while True:
    num = int(input("Digite un num entero: "))
    if num == 0:
        break
    else:
        suma += num

print(f"La suma fue: {suma}")
