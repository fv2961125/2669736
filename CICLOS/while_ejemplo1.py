# Pedir 3 números y averiguar si son impares o no.
# Usar ciclo while.

cont = 1
while cont <= 3:
    print(cont)
    num = int(input("Digite un núm: "))
    if num % 2 != 0:
        print(f"El número {num} es impar!!")
    else:
        print("No es impar.")

    cont += 1
