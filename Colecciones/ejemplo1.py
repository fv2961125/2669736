""" Ejercicio 1.
Ofrecer al usuario un menú de opciones:
1. Crear deportista
2. Eliminar deportista
3. Ver datos de 1 deportista.
4. Salir
Notas:
- Crear deportistas usando diccionarios.
        (nombre, edad, deporte, pago_hora, tiempo_invertido)
- Ver datos:
    Preguntar por el nombre del deportista:
    Mostrar: Deporte, edad y sueldo (pago_hora x tiempo_invertido)
"""

deportistas = []

while True:
    op = int(input("""
    1. Crear deportista
    2. Eliminar deportista
    3. Ver datos de 1 deportista.
    4. Salir
    """))

    if op == 4:
        print("Hasta luego!!")
        break
    elif op == 1:
        nombre = input("Digite el nombre del deportista: ")
        edad = int(input("Digite la edad del deportista: "))
        deporte = input("Digite el deporte practicado por el deportista: ")
        pago_hora = int(input("Digite el $ pago por hora del deportista: "))
        tiempo = int(input("Digite el tiempo invertido por el deportista: "))
        datos = {
            "nombre": nombre,
            "edad": edad,
            "deporte": deporte,
            "pago_hora": pago_hora,
            "tiempo": tiempo
        }
        for d in deportistas:
            if nombre == d["nombre"]:
                print("Ya existe un deportista con ese nombre....")
                break
        else:
            print("Deportista creado correctamente!!")
            deportistas.append(datos)
        print(deportistas)
    elif op == 2:
        print(deportistas)
        persona = input("Digite el nombre del deportista a eliminar: ")
        count = 0
        for d in deportistas:
            if persona == d["nombre"]:
                deportistas.pop(count)
                print("Deportista eliminado correctamente")
                break
            count += 1
        else:
            print("Deportista no encontrado...")
        print(deportistas)
    elif op == 3:
        print(deportistas)
        persona = input("Digite el nombre del deportista a consultar: ")
        for d in deportistas:
            if persona == d["nombre"]:
                print(f"""
                Deporte: {d["deporte"]} 
                Edad: {d["edad"]} 
                Sueldo: ${d["pago_hora"]*d["tiempo"]} 
                """)
                break
        else:
            print("Deportista no encontrado...")

