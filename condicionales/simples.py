"""Operadores lógicos

<       menor que
>       mayor que
<=      menor o igual que
>=      mayor o igual que
==      igual a
!=      diferente de
and     y
or      o
not     negación, si algo es Cierto lo pasa a Falso
                  y viceversa.
"""
# Condicionales Simples

#   if <condicion>:
#       pass

# --- Averiguar si una persona es mayor de edad.

edad = int(input("Cual es su edad? "))
if edad >= 18:
    print("La persona si es mayor de edad.")

# Formateo, indentacion, identacion, sangría, tabulación

