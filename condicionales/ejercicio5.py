# Averiguar si un número dado por pantalla, es factor de 2 y 8. Si no es así, entonces multiplicarlo por 5 y averiguar si es factor de 10.

# caracteres de escape  

# \n   --> salto de línea o enter o retorno de carro
# \t   --> tabular
# \'   --> imprimir comillas sencillas dentro de sencilla
# \"   --> imprimir comillas dobles dentro de doble
# \\   --> imprimir backslash \ siendo caracter de escape
print('Hola\\ "don\'t"....')

num = int(input("Digite un número:\n"))

if (num % 2 == 0) and (num % 8 == 0):
    print("Si es factor de 2 y 8")
else:
    num = num * 5
    if num % 10 == 0:
        print("Multiplicado por cinco, si es factor de 10")
    else:
        print("No es factor de diez")







