"""Preguntar al usuario su deporte favorito:
1. Fútbol
2. Voley
3. Basquet
4. Sóftbol

Ofrecer una pregunta de conocimiento al usuario, dependiendo de cada deporte. Así:
--- Este deporte tiene 6 jugadores ? SI/NO
    Decir. Si es correcto o no.
"""
print("""Cuál es su deporte favorito:
1. Fútbol
2. Voley
3. Basquet
4. Sóftbol
""")
opcion = int(input("Escoja una opción: "))

if opcion == 1:
    respuesta = input("Este deporte tiene 11 jugadores ? SI/NO: ")
    if respuesta == "SI":
        print("Es Correcto!!")
    else:
        print("No es correcto. Intente de nuevo.")
elif opcion == 2:
    respuesta = input("Este deporte tiene 6 jugadores ? SI/NO: ")
    if respuesta == "SI":
        print("Es Correcto!!")
    else:
        print("No es correcto. Intente de nuevo.")
elif opcion == 3:
    respuesta = input("Este deporte tiene 5 jugadores ? SI/NO: ")
    if respuesta == "SI":
        print("Es Correcto!!")
    else:
        print("No es correcto. Intente de nuevo.")
elif opcion == 4:
    respuesta = input("Este deporte tiene 10 jugadores ? SI/NO: ")
    if respuesta == "SI":
        print("Es Correcto!!")
    else:
        print("No es correcto. Intente de nuevo.")
else:
    print("Opción incorrecta.")