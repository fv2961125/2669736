# Construir un algoritmo que convierta a positivo un número negativo digitado por pantalla.

#    -3      -3*-1      3
#    -20     -20*-1     20
    
num = int(input("Digite un número negativo: "))
if num < 0:
    pos = num * -1
    print(f"El número {num} convertido a positivo: {pos}")
else:
    pos = num
    print(f"El número {num} convertido a positivo: {pos}")
