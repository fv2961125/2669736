class Edificio:
	tipo = "Residencial"

	def __init__(self, nombre, pisos):
		self.nombre = nombre
		self.pisos = pisos

		self.terminado = True
		self.color = "blanco"

	def pintar(self, color):
		self.color = color

	def correr(self):
		print("Soy un edificio y CORRER me encanta!!")


if __name__ == "__main__":
	# crear objetos: Instancias de Edificio
	ob1 = Edificio("Cantasia", 5)
	ob2 = Edificio("Aquavento", 19)
	print(f"Nombre: {ob1.nombre} y Cant. Pisos: {ob1.pisos}")
	print(f"Color: {ob1.color}")

	print(f"Nombre: {ob2.nombre} y Cant. Pisos: {ob2.pisos}")
	ob3 = Edificio("Suramericana", 24)
	print(f"Nombre: {ob3.nombre}")

	ob1.pintar("Beige")
	print(f"Nuevo Color: {ob1.color}")

	ob1.color = "Verde"
	print(f"Nuevo Color: {ob1.color}")
