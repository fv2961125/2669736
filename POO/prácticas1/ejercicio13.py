class Consumo:
	precio_gasolina_litro = 3830

	def __init__(self, kms, litros, vmed):
		self.kms_recorridos = kms
		self.litros_consumidos = litros
		self.velocidad_media = vmed

	def get_tiempo(self):
		return self.kms_recorridos / self.velocidad_media

	def consumo_medio(self):
		return self.kms_recorridos / (100 * self.litros_consumidos)

	def consumo_euros(self):
		return (self.kms_recorridos * Consumo.precio_gasolina_litro) / 100


if __name__ == "__main__":
	km_r = float(input("Digite la cantidad de Kilómetros recorridos: "))
	ltrs = float(input("Digite la cantidad de Litros consumidos en el recorrido: "))
	veloc = int(input("A que velocidad viajó (kms/hora): "))

	veh1 = Consumo(km_r, ltrs, veloc)

	print(f"""
	Datos del vehículo:
	Kilómetros Recorridos: {veh1.kms_recorridos}
	Consumo en litros por Recorrido: {veh1.litros_consumidos}
	Velocidad Media: {veh1.velocidad_media}
	
	Precio de la Gasolina:
	${Consumo.precio_gasolina_litro}
	""")
	print("*"*30,"Resultados", "*"*30)

	print(f"Tiempo gastado: {veh1.get_tiempo():0.1f} horas")
	print(f"Consumo medio por cada 100 litros: {veh1.consumo_medio()} km/litros")
	print(f"Costo Consumo medio por cada 100 litros: ${veh1.consumo_euros():.0f} -> km/litros")

	print("*" * 30, "Resultados", "*" * 30)