import time
class Coche:

	def __init__(self):
		self.__velocidad = 0

	def get_velocidad(self):
		return self.__velocidad

	def acelera(self, mas):
		if mas > 0:
			self.__velocidad += mas
		else:
			print("No se pudo aumentar la velocidad...")

	def frena(self, menos):
		if menos > 0 and menos <= self.__velocidad:
			self.__velocidad -= menos
		else:
			print("No se pudo disminuir la velocidad...")


if __name__ == "__main__":
	carro1 = Coche()
	a = []
	for v in range(1, 240, 20):
		a.append(Coche())
		time.sleep(1)
		if carro1.get_velocidad() == 0:
			print("El coche está parado.")
		else:
			print(f"El coche va a: {carro1.get_velocidad()} km/h")
		print(f"Subir {v}")
		carro1.acelera(v)

		if carro1.get_velocidad() >= 240:
			carro1.frena(carro1.get_velocidad())
			break


	if carro1.get_velocidad() == 0:
		print("El coche está parado.")
	else:
		print(f"El coche va a: {carro1.get_velocidad()} km/h")