"""
Construir un programa que permita que permita identificar en un edificio de 5 pisos, el lugar en el que se encuentra un empleado de una empresa.
Los empleados se deben registrar. Lo cual da lugar a un carnet con su nombre y un número de ID de 3 dígitos único (controlar).
Usar el siguiente diccionario para el registro:
empleados = {
	"nombre": ["Andrea", "Pedro"],
	"cargo": ["Gerente", "Contador"],
	"id": [789, 123]
}

# Nota: cada piso tiene 3 oficinas:

ubicacion= {
	"empleado": [789]
	"piso": [5]
	"oficina": [1]
}

# Esto quiere decir que Andrea (la Gerente), se encuentra en la primera oficina del piso 5.

# Los empleados pueden ir a otras oficinas usando su carnet, para ello se debe indicar a que lugar va el empleado,
# Ejemplo: ID: 789, que es Andrea va al piso: 2, oficina: 3. Entonces se debe hacer el siguiente moviento de información.

ubicacion= {
	"empleado": [789, 789]
	"piso": [5, 2]
	"oficina": [1, 3]
}

# Si ahora Pedro va a otro lugar. Ejemplo: ID: 123 va al piso: 5, oficina: 1. Entonces se debe hacer el siguiente moviento de información.

ubicacion= {
	"empleado": [789, 789, 123]
	"piso": [5, 2, 5]
	"oficina": [1, 3, 1]
}

Nota: Todos los movimientos se registran en este diccionario. A partir de éste se puede consultar todo el historial de un empleado.
================================================================================================================================
El sistema debe tener el siguiente menu:

- Registrar empleado		-> usar función: el empleado debe quedar en el diccionario "empleado"
- Mover empleado			-> usar función: sólo puede mover personas que existan en el diccionario "empleados"
											 dejar registro del movimiento en el diccionario "ubicacion"

Reportes (use funciones):
- Saber el último lugar en el que ha estado un empleado a través de su ID. Nota: si no tiene movimientos, indicarlo.
		Ejemplo: ID=789 ->	Andrea está en el piso 2, oficina 3.
				ID= 123 ->	Pedro está en el piso 5, oficina 1.
- Saber todos los movimientos de un empleado.
		Ejemplo:	ID=789
					Andrea estuvo en el piso 5, oficina 1.
					Andrea estuvo en el piso 2, oficina 3.
					...
					Andrea está en el piso Z, oficina X.

"""
