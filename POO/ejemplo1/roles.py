class Grupo:
	conf = {
		"admin": ["leer", "editar", "eliminar", "crear"],
		"secretaria": ["leer", "editar", "crear"],
		"usuario": ["leer"]
	}

	def __init__(self, tipo):
		if tipo in Grupo.conf:
			self.tipo = tipo
			self.permisos = Grupo.conf[tipo]
		else:
			self.tipo = ""
			self.permisos = []

	def cambiar_grupo(self, tipo):
		if tipo in Grupo.conf:
			self.tipo = tipo
			self.permisos = Grupo.conf[tipo]
		else:
			self.tipo = ""
			self.permisos = []

	# modificadores de acceso, método privado para agregar más roles al dict.

if __name__ == "__main__":
	print("Ejecute el main.py por favor....")