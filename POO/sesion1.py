class Edificio:
	# atributos de clase (global)
	tipo = "Residencial"	# "Comercial" ó "Residencial"

	# método inicializador o constructor
	def __init__(self):
		# atributos de instancia, objeto
		self.nombre = ""
		self.terminado = True
		self.pisos = -1
		self.color = "blanco"

	# métodos, comportamiento o acción de un objeto de esta clase
	def caminar(self):
		print("Soy un edificio y estoy caminando...")

	def correr(self):
		print("Soy un edificio y CORRER me encanta!!")


if __name__ == "__main__":
	# crear objetos: Instancias de Edificio
	ob1 = Edificio()
	ob2 = Edificio()
