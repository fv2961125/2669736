# Solicitar el nombre del cliente
# Pedir 3 productos (nombre y precio), calcular el promedio de ellos.
# Mostrar todos los nombres y precios y el promedio global. Con saludo al cliente incluído.

# Entradas
nombre = input("Digite su nombre:")
pro1nombre = input("Nombre del producto 1: ")
pro1precio = int( input("Precio del producto 1: ") )
pro2nombre = input("Nombre del producto 2: ")
pro2precio = int( input("Precio del producto 2: ") )
pro3nombre = input("Nombre del producto 3: ")
pro3precio = int( input("Precio del producto 3: ") )

# Proceso
promedio = (pro1precio + pro2precio + pro3precio) / 3

# Salida
print("Gracias por su compra", nombre)
print("Producto 1:")
print("Nombre", pro1nombre, ", Precio: ", pro1precio)
print("Producto 2:")
print("Nombre", pro2nombre, ", Precio: ", pro2precio)
print("Producto 3:")
print("Nombre", pro3nombre, ", Precio: ", pro3precio)
print("El promedio global es: ", promedio)
